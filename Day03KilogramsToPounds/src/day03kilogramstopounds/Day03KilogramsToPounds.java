/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03kilogramstopounds;

/**
 *
 * @author john
 */
public class Day03KilogramsToPounds {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double kilograms = 1;
        double pounds = 1;
        System.out.printf("%-12s %-12s \n", "Kilograms", "Pounds");
        for (int i = 0; i < 99; i++){
            pounds = kilograms * 2.2;
            System.out.printf("%-12d %-12.1f \n",(int)kilograms, pounds);
            kilograms += 2;
        }
    }
    
}
