package dsday01carssorted;

public class Car implements Comparable<Car>{

    String makeModel;
    double engineSize;
    int prodYear;

    public Car(String makeModel, double engineSize, int prodYear) {
        this.makeModel = makeModel;
        this.engineSize = engineSize;
        this.prodYear = prodYear;
    }
    
    public Car(String dataLine) {
        String[] data = dataLine.split(";");
        this.makeModel = data[0];
        this.engineSize = Double.parseDouble(data[1]);
        this.prodYear = Integer.parseInt(data[2]);
    }

    @Override
    public String toString() {
        return prodYear + " " + makeModel + " with size " + engineSize + " engine.";
    }

    @Override
    public int compareTo(Car o) {
        if(this.makeModel.compareTo(o.makeModel) > 0){
            return 1;
        }else if(this.makeModel.compareTo(o.makeModel) < 0){
            return -1;
        }
        return 0;
    }
}
