package dsday01carssorted;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class DSDay01CarsSorted {

    static ArrayList<Car> parking = new ArrayList<Car>();
    static String path = "carsInput.txt";
    public static void LoadDataFromFile(){
        try(Scanner reader = new Scanner(new File(path))){
            while(reader.hasNextLine()){
                String dataLine = reader.nextLine();
                parking.add(new Car(dataLine));
            }
        }catch(IOException ex){
            System.out.println("Error: " + ex.getMessage());
        }
    }
    
    
    public static void main(String[] args) {
        LoadDataFromFile();
/*
        parking.add(new Car("Toyota", 20.50, 2012));
        parking.add(new Car("Honda", 18.55, 2016));
        parking.add(new Car("Ferrari", 24.00, 2021));
        parking.add(new Car("Porche", 23.50, 2018));
        parking.add(new Car("Audi", 18.90, 1995));
        parking.add(new Car("Volkswagen", 18.90, 1995));
        parking.add(new Car("Mazda", 18.90, 1995));
        parking.add(new Car("Kia", 16.50, 2009));
        //Collections.sort(parking);
        //Collections.reverse(parking);
        /*
        Comparator<Car> com = new Comparator<Car>(){
            public int compare(Car c1, Car c2){
                if(c1.engineSize > c2.engineSize){
                    return 1;
                }else if(c1.engineSize < c2.engineSize){
                    return -1;
                }
                return 0;
            }
        };
        Collections.sort(parking, com);
         */
        //Sorting with COMPARATOR by YEAR and MAKEMODEL
        Comparator<Car> comByYear = new Comparator<Car>(){
            public int compare(Car c1, Car c2){
                if(c1.prodYear == c2.prodYear){
                    if(c1.makeModel.compareTo(c1.makeModel) > 0){
                        return 1;
                    }else if(c1.makeModel.compareTo(c2.makeModel) < 0){
                        return -1;
                    }
                    else return 0;
                }
                if(c1.prodYear > c2.prodYear){
                    return 1;
                }else if(c1.prodYear < c2.prodYear){
                    return -1;
                }
                else return 0;
            }
        };
        Collections.sort(parking, comByYear);
        //sorting with name LAMBDA
        parking.sort((Car c1, Car c2) -> c1.makeModel.compareTo(c2.makeModel));
        for (Car c : parking) {
            System.out.println(c);
        }
    }

}
