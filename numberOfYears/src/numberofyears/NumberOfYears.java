/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numberofyears;
import java.util.Scanner;
/**
 *
 * @author john
 */
public class NumberOfYears {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter minutes: ");
        double minutes = input.nextDouble();
        if (minutes < 0){
            System.out.println("wrong input can't enter negative values");
            System.exit(1);
        }
        double hours = minutes / 60;
        double days = hours / 24;
        double years = days / 365;
        double daysRemainder = days % 365;
        System.out.println(minutes + " minutes is " + years + " years and " + daysRemainder + " days.");
    }
    
}
