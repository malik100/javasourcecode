
package day02people;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidParameterException;

public class Person {

    public Person(String name, int age) {
        setName(name);
        setAge(age);
    }
    
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(name.length() < 2 || name.length() > 20){
            throw new InvalidParameterException("Name must be 2-20 characters");
        }
        this.name = name;
        
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
       if(age < 1 || age > 150){
           throw new InvalidParameterException("Age must be between 1-150");
       }
       this.age = age;
    }
    
    public String toString() {
        return this.name + "::" + this.age + "   ";
    }
}
