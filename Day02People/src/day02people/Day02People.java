
package day02people;

import java.io.File;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Day02People {

    static ArrayList<Person> people = new ArrayList<>();
    
    public static void main(String[] args) {
        int counter = 0;
        try(Scanner read = new Scanner (new File ("people.txt"))){
            while(read.hasNextLine()){
                String[] peopleData = read.nextLine().split(";");
                try{
                    if(peopleData.length == 2){
                        people.add(new Person(peopleData[0], Integer.parseInt(peopleData[1])));
                    }
                }catch(InvalidParameterException ip){
                    System.out.println("Wrong data to initialize person (Length or Type) LINE SKIPPED");
                    continue;
                }catch(NumberFormatException w){
                    System.out.println("Wrong data type for age (Not Integer) SKIPPING");
                    continue;
                }
                
            }
        }catch(IOException i){
            System.out.println("Error Reading file");
        }
        
        for(Person ppl : people){
            System.out.println("\n" + ppl);
        }
    }
    
}
