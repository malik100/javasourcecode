
package dsday03genericslinkedlist;

import java.util.Objects;

 class Person{
        String name;
        int age;

        public Person(String name, int age) {
            this.name = name;
            this.age = age;
        }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.name);
        hash = 79 * hash + this.age;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        if (this.age != other.age) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

        
        @Override
        public String toString() {
            return "Person{" + "name=" + name + ", age=" + age + '}';
        }
        
    }

public class DSDay03GenericsLinkedList {

    
    public static void main(String[] args) {
        CustomGenericList<Person> list = new CustomGenericList<>();
        list.add(new Person("Joe", 50));
        list.add(new Person("Mary", 35));
        System.out.println(list);
        list.insertValueAtIndex(new Person("Ken", 66), 1);
        list.deleteByValue(new Person("Ken", 66));
        System.out.println(list);
    }
    
}
