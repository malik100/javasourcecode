/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compoundvalue;

import java.util.Scanner;

/**
 *
 * @author john
 */
public class CompoundValue {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a savings amount value: ");
        double monthlySavings = input.nextDouble();
        if (monthlySavings < 0){
            System.out.println("wrong input can't enter negative values");
            System.exit(1);
        }
        double savingsCalculation = monthlySavings  * (1 + 0.00417); //after month 1
        double savingsFinal = (monthlySavings + savingsCalculation) * (1 + 0.00417); //after month 2
        savingsFinal = (monthlySavings + savingsFinal) * (1 + 0.00417); //after month 3
        savingsFinal = (monthlySavings + savingsFinal) * (1 + 0.00417); //month 4
        savingsFinal = (monthlySavings + savingsFinal) * (1 + 0.00417); //month 5
        savingsFinal = (monthlySavings + savingsFinal) * (1 + 0.00417); //month 6
        System.out.println(savingsCalculation);
        System.out.println(savingsFinal);
    }
    
}
