
package dsday08binarytreegeneric;
class Person{
    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" + "name=" + name + ", age=" + age + '}';
    }
    
    
}


public class DSDay08BinaryTreeGeneric {

    
    public static void main(String[] args) {
        BinaryTreeGen<String, Double> bt = new BinaryTreeGen<>();
        /*
        bt.put("g", 90.0);
        bt.put("a", 33.0);
        bt.put("c", 66.0);
        bt.put("i", 12.0);
        bt.put("h", 5.0);
        bt.put("b", 4.0);
        bt.put("d", 100.0);
*/
        bt.put("g", 90.0);
        bt.put("a", 33.0);
        bt.put("c", 66.0);
        bt.put("i", 12.0);
        bt.put("h", 5.0);
        bt.put("b", 4.0);
        bt.put("d", 100.0);
        System.out.println("NodesCount: " + bt.nodesCount);
        BinaryTreeGen.Pair[] p = bt.getKeyValuePairsInOrder(new BinaryTreeGen.Pair[5]);
        for(BinaryTreeGen.Pair pair : p){
            System.out.printf("%s------%s\n", pair.key, pair.value);
        }
        System.out.println("");
        bt.getValByKey("b");
        
        BinaryTreeGen.Pair[] x = bt.getKeyValuePairsInOrder(new BinaryTreeGen.Pair[1]);
        for (int i = 0; i < x.length; i++) {
            System.out.println(x[i].key + "   " + x[i].value);
        }
        
        System.out.println("");
        for (BinaryTreeGen.Pair iter : bt){
            System.out.println(iter.key + " <---> " + iter.value);
        }
        
        System.out.println("");
        
        BinaryTreeGen<Integer, Person> tree = new BinaryTreeGen();
        
        tree.put(50, new Person("Mary", 33));
        tree.put(100, new Person("Joe", 44));
        tree.put(25, new Person("Louis", 20));
        tree.put(200, new Person("Ken", 20));
        
        for(BinaryTreeGen.Pair zz : tree){
            System.out.println(zz.key.toString() + " " + zz.value.toString());
        }
        
        System.out.println(tree.root.left.value);
    }
    
}
