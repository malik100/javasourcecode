package dsday08binarytreegeneric;

import java.lang.reflect.Array;
import java.util.Iterator;

public class BinaryTreeGen<K extends Comparable, V> implements Iterable<BinaryTreeGen.Pair> {

    class IterateHolder implements Iterator<Pair>{
        Pair[] pairArray;
        int index;

        public IterateHolder(Pair[] pairArray) {
            this.pairArray = pairArray;
        }
        
        
        @Override
        public boolean hasNext() {
            return index < pairArray.length;
        }

        @Override
        public Pair next() {
            return pairArray[index++];
        }
        
    }
    
    
    @Override
    public Iterator iterator() {
        return new IterateHolder(getKeyValuePairsInOrder(new BinaryTreeGen.Pair[1]));
    }

    

    public class Node {

        K key;
        V value;
        Node left, right;

        public Node(K key, V value, Node left, Node right) {
            this.key = key;
            this.value = value;
            this.left = left;
            this.right = right;
        }

    }

    class Pair {

        K key;
        V value;

        
        public Pair(K key, V value) {
            this.key = key;
            this.value = value;
        }

    }

    public Node root;
    public int nodesCount;

    // if put attempts to put a key that already exists then value is updated (no exception is thrown)
    // values may be duplicates but keys may not
    void put(K key, V value) {
        Node newNode = new Node(key, value, null, null);
        if (nodesCount == 0) {
            root = newNode;
            nodesCount++;
            return;
        }
        Node current = root;
        while (current != null) {
            if (newNode.key.compareTo(current.key) == 0) {
                current.value = value;
                return;
            }
            if (newNode.key.compareTo(current.key) > 0 && current.right == null) {
                current.right = newNode;
                nodesCount++;

                return;
            }
            if (newNode.key.compareTo(current.key) < 0 && current.left == null) {
                current.left = newNode;
                nodesCount++;
                return;
            }
            current = newNode.key.compareTo(current.key) > 0 ? current.right : current.left;
        }

    }

    // print out all key-value pairs (one per line) from the smallest key to the largest
    void printAllKeyValPairs() {
        keyHelper(root, 0);
    }
    /*
    private void printHelper(Node n) {
        if (n == null) {
            return;
        }
        if (n.left != null) {
            printHelper(n.left);
        }
        System.out.println(">>> " + n.key);
        if (n.right != null) {
            printHelper(n.right);
        }
    }
     */
    private int index;
    private K[] arr;
    private Pair[] pairArr;

    private void keyHelper(Node n, int caller) {
        if (n == null) {
            return;
        }
        if (n.left != null) {
            keyHelper(n.left, caller);
        }
        //----------------------------------------------------------------------
        if (caller == 1) {
            arr[index] = n.key;
            index++;
        } else if (caller == 2) {
            pairArr[index] = new Pair(n.key, n.value);
            index++;
        } else {
            System.out.println(n.key + " <><><>");
        }
        //----------------------------------------------------------------------
        if (n.right != null) {
            keyHelper(n.right, caller);
        }

    }

    public void getValByKey(K key) {
        if (root == null) {
            return;
        }
        Node current = root;
        while (true) {
            if(current == null){
                System.out.println("Not Found");
                return;
            }
            if (current.key.toString().equals(key.toString())) {
                System.out.println("Found: " + current.key.toString() + " - " + current.value.toString());
                return;
            }
            current = key.toString().compareTo(current.key.toString()) > 0 ? current.right : current.left;
        }

    }

    // return all keys from smallest to largest (alphabetically)
    K[] getKeysInOrder() {
        index = 0;
        //Class type = x.getClass().getComponentType().getComponentType();
        arr = (K[]) Array.newInstance(root.key.getClass(), nodesCount);
        //arr = (K[]) java.lang.reflect.Array.newInstance(template.getClass().getComponentType(), nodesCount);
        keyHelper(root, 1);
        return arr;
    }

    // return pairs of key-value in order of keys
    Pair[] getKeyValuePairsInOrder(Pair[] template) {
        index = 0;
        //pairArr = (Pair[]) Array.newInstance(Pair.getClass(), nodesCount);
        pairArr = (Pair[]) java.lang.reflect.Array.newInstance(template.getClass().getComponentType(), nodesCount);
        keyHelper(root, 2);
        return pairArr;
    }
}
