/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package windchill;

import java.util.Scanner;

/**
 *
 * @author john
 */
public class WindChill {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double windChill;
        double temperature;
        double windSpeed;
        System.out.println("Enter a temperature between -58Fahrenheight and 41Fahrenheight: ");
        temperature = input.nextDouble();
        System.out.println("Enter a windspeed 2 miles per hour or higher: ");
        windSpeed = input.nextDouble();
        if (temperature < -58 || temperature > 41){
            System.out.println("wrong input enter a temperature between -58 and 41 degrees Fahrenheight");
            System.exit(1);
        }
        if (windSpeed < 2){
            System.out.println("wrong input enter a a wind speed 2MPH or higher");
            System.exit(1);
        }
        windChill = 35.74 + (0.6215 * temperature) - (35.75 * Math.pow(windSpeed, 0.16 ))
                + (0.4275 * (temperature * (Math.pow(windSpeed, 0.16 ))));
        System.out.println(windChill);
    }
    
}
