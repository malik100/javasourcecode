package dsday01arraysearch;

import java.util.Arrays;

public class DSDay01ArraySearch {

    public static void main(String[] args) {
        int[][] data2D = {
            {1, 3, 6, 8},
            {7, 1, 2, 3},
            {8, 3, 2, 1},
            {1, 7, 1, 9},};
        
        int[][] arr = duplicateArray(data2D);
        System.out.println(Arrays.deepToString(arr));
    }

    public static int getIfExists(int[][] data, int row, int col) {
        // If exists, return the element, otherwise return 0
        if(data.length < row + 1 || row < 0){
            return 0;
        }
        if(data[row].length < col + 1 || col < 0){
            return 0;
        }
        return 1;
    }

    public static int sumOfCross(int[][] data, int row, int col) {
        int sum = data[row][col];
        int aboveY = row - 1;
        int aboveX = col;
        if(getIfExists(data, aboveY, aboveX) > 0){sum += data[aboveY][aboveX];}
        int belowY = row + 1;
        int belowX = col;
        if(getIfExists(data, belowY, belowX) > 0){sum += data[belowY][belowX];}
        int leftY = row;
        int leftX = col -1;
        if(getIfExists(data, leftY, leftX) > 0){sum += data[leftY][leftX];}
        int rightY = row;
        int rightX = col +1;
        if(getIfExists(data, rightY, rightX) > 0){sum += data[rightY][rightX];}
        // return sum of the element at row/col
        // plus (if they exist) element above, below, to the left and right of it
        return sum;
    }
    
    public static int[][] duplicateArray(int[][] firstArr){
        int[][] secondArr = new int[firstArr.length][firstArr[0].length];
        for (int row = 0; row < firstArr.length; row++) {
            for (int col = 0; col < firstArr[row].length; col++) {
                secondArr[row][col] = sumOfCross(firstArr, row, col);
            }
        }
        return secondArr;
    }
}
