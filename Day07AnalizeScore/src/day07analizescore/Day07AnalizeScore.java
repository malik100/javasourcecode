
package day07analizescore;

import java.util.ArrayList;
import java.util.Scanner;


public class Day07AnalizeScore {

    static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) {
        ArrayList<Integer> scores = new ArrayList<>();
        while(true){
            int value = input.nextInt();
            if(value < 0){
                break;
            }
            scores.add(value);
        }
        int average= 0;
        for (int i = 0; i < scores.size(); i++) {
            average += scores.get(i);
        }
        average = (int)((double)average / scores.size());
        int aboveAvg = 0;
        int belowAvg = 0;
        for (int i = 0; i < scores.size(); i++) {
            if (scores.get(i) > average){
                aboveAvg++;
            }else{
                belowAvg++;
            }
        }
        System.out.printf("average was: %d the number of itmes below aveage are: %d and above average are: %d",average, belowAvg, aboveAvg);
        
        
    }  
}
