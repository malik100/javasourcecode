/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03decimaltohex;

/**
 *
 * @author john
 */
public class DAY03DecimalToHex {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int value = 15;  //0-15
        char c;
        if (value <= 9){
            c = (char)('0' + value);
        }else{
            c = (char)('A' + value - 10);
        }
        System.out.println("Hexadecimal: " + c);
    }
    
}
