
package day10library;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class Database {
    private static final String dbURL = "removed for security";
    private static final String username = "removed for security";
    private static final String password = "removed for security";

    private Connection conn;

    public Database() throws SQLException {
        conn = DriverManager.getConnection(dbURL, username, password);
    }

    public ArrayList<Book> getAllBooks() throws SQLException, DataInvalidException {
        ArrayList<Book> list = new ArrayList<>();
        String sql = "SELECT id, isbn, titleAndAuthor from books";
        PreparedStatement statement = conn.prepareStatement(sql);
        try (ResultSet result = statement.executeQuery(sql)) {
            while (result.next()) {
                int id = result.getInt("id");
                String isbn = result.getString("isbn");
                String titleAndAuthor = result.getString("titleAndAuthor");
                list.add(new Book(id, isbn, titleAndAuthor, null));     //DataInvalid EX
            }
        }
        return list;
    }

    public Book getBookById(int id) throws SQLException, DataInvalidException {
        PreparedStatement stmtSelect = conn.prepareStatement("SELECT * from books where id=?");
        stmtSelect.setInt(1, id);
        try (ResultSet resultSet = stmtSelect.executeQuery()) {
            if (resultSet.next()) {
                String isbn = resultSet.getString("isbn");
                String titleAndAuthor = resultSet.getString("titleAndAuthor");
                byte[] coverImage = resultSet.getBytes("coverImage");
                return new Book(id, isbn, titleAndAuthor, coverImage);      //DataInvalid EX
            } else {
                throw new SQLException("Record not found");
            }
        }
    }

    public void addBook(Book book) throws SQLException {
        String sql = ("insert into books values (null, ?, ?, ?)");
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, book.getIsbn());
        statement.setString(2, book.getTitleAndAuthor());
        statement.setBytes(3, book.getCoverImage());
        statement.executeUpdate();  //for insert update delete
        System.out.println("Record inserted");
    }
    
    public void modifyBook(Book book) throws SQLException{
        String sql = "update books set isbn=?, titleAndAuthor=?, coverImage=? where id=?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, book.getIsbn());
        statement.setString(2, book.getTitleAndAuthor());
        statement.setBytes(3, book.getCoverImage());
        statement.setInt(4, book.getId());
        statement.executeUpdate();
        System.out.println("Record updated id =" + book.getId());
    }
    
    public void deleteBook(Book book) throws SQLException{
        String sql = "delete from books where id=?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, book.getId());
        statement.executeUpdate();
        System.out.println("Record deleted id =" + book.getId());
    }
}
