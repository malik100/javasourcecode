
package day10library;


public class Book {
    private int id;
    private String isbn;
    private String titleAndAuthor;
    private byte[] coverImage;
//Constructor-------------------------------------------------------------------

    public Book(int id, String isbn, String titleAndAuthor, byte[] coverImage) throws DataInvalidException {
        setId(id);
        setIsbn(isbn);
        setTitleAndAuthor(titleAndAuthor);
        setCoverImage(coverImage);
    }
    
//Getters-----------------------------------------------------------------------
    public int getId() {
        return id;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getTitleAndAuthor() {
        return titleAndAuthor;
    }

    public byte[] getCoverImage() {
        return coverImage;
    }
    
//Setters-----------------------------------------------------------------------
    public void setId(int id) throws DataInvalidException {
        if(id < 0){
            throw new DataInvalidException("ID can't be a negative number");
        }
        this.id = id;
    }

    public void setIsbn(String isbn) throws DataInvalidException {
        if(!isbn.matches("[0-9-]{10,20}X?")){
            throw new DataInvalidException("ISBN can only be numbers and hyphens"
                    + " with an optional X at the end");
        }
        this.isbn = isbn;
    }

    public void setTitleAndAuthor(String titleAndAuthor) throws DataInvalidException {
        if(titleAndAuthor.length() < 2 || titleAndAuthor.length() > 200){
            throw new DataInvalidException("Author and Title must be between "
                    + "2-200 characters");
        }
        this.titleAndAuthor = titleAndAuthor;
    }

    public void setCoverImage(byte[] coverImage) {
        this.coverImage = coverImage;
    }
    //ToString------------------------------------------------------------------

    @Override
    public String toString() {
        return  String.format("%s: %s ISBN: %s", id, titleAndAuthor, isbn);
    }
    
}
