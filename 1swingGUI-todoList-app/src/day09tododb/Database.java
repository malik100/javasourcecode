
package day09tododb;
import static day09tododb.Day09TodoDb.simpleDate;
import java.sql.*;
import java.util.ArrayList;

    
public class Database {
    private static final String dbURL = "removed for security";
    private static final String username = "removed for security";
    private static final String password = "removed for security";
    
    private Connection conn;
    public Database() throws SQLException{
        conn = DriverManager.getConnection(dbURL, username, password);
    }
    
    public ArrayList<Todo> getAllTodos() throws SQLException, DataInvalidException{
        ArrayList<Todo> todos = new ArrayList<>();
        String sql = "SELECT * from todo";
        PreparedStatement statement = conn.prepareStatement(sql);
        try(ResultSet result = statement.executeQuery(sql)){
            while(result.next()){
                int id = result.getInt("id");
                String task = result.getString("task");
                int difficulty = result.getInt("difficulty");
                Date dueDate = result.getDate("dueDate");
                String dateStr = simpleDate.format(dueDate);
                String status = result.getString("status");
                todos.add(new Todo(id, task, difficulty, dateStr, status)); //Datainvalid ex
            }
        }
        return todos;
    }
    
    public void addTodo(Todo todo) throws SQLException{
        String sql = "INSERT into todo values (null, ?, ?, ?, ?)";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, todo.task);
        statement.setInt(2, todo.difficulty);
        java.sql.Date sqlDate = new java.sql.Date(todo.dueDate.getTime()); //conversion
        statement.setDate(3, sqlDate);
        statement.setString(4, todo.status.toString());
        statement.executeUpdate();
    }
    public void modifyTodo(Todo todo) throws SQLException{
        String sql = "UPDATE todo set task=?, difficulty=?, dueDate=?, status=? where id=?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, todo.task);
            statement.setInt(2, todo.difficulty);
            String dateStr = simpleDate.format(todo.dueDate);
            statement.setString(3, dateStr);
            statement.setString(4, todo.status.toString());
            statement.setInt(5, todo.id);
            statement.executeUpdate();
            System.out.println("Record updated id =" + todo.id);
    }
}
