
package day09tododb;

import static day09tododb.Day09TodoDb.simpleDate;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


public class Todo {
    //fields and enum-----------------------------------------------------------
    int id;
    String task;
    int difficulty;
    Date dueDate;
    Status status;
    
    enum Status{
        PENDING, DONE, DELEGATED
    }
    //constructor---------------------------------------------------------------
    public Todo(int id, String task, int difficulty, String dueDate, String status) throws DataInvalidException {
        setId(id);
        setTask(task);
        setDifficulty(difficulty);
        setDate(dueDate);
        setStatus(status);
    }
    //Setters with Validation---------------------------------------------------
    public void setId(int id) throws DataInvalidException{
        if(id < 0){
            throw new DataInvalidException("Id must not be a negative number");
        }
        this.id = id;
    }
    public void setTask(String task) throws DataInvalidException{
        if(!task.matches("[a-zA-Z _*?%#@(),./\\-]{1,100}")){
            throw new DataInvalidException("Task must be 1-100 characters and must only contain"
                    + " _*?%#@(),./\\- characters");
        }
        this.task = task;
    }
    public void setDifficulty(int difficulty) throws DataInvalidException{
        if(difficulty > 5 || difficulty < 1){
            throw new DataInvalidException ("Difficulty must be between 1-5");
        }
        this.difficulty = difficulty;
    }
    public void setDate(String dateStr) throws DataInvalidException{
        try {
            Date date = simpleDate.parse(dateStr);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            int year = cal.get(Calendar.YEAR);
            if(year < 1900 || year > 2100){
                throw new DataInvalidException("Year must be between 1900-2100");
            }
            this.dueDate = date;
        } catch (ParseException ex) {
            throw new DataInvalidException("Error parsing date");
        }
    }
    public void setStatus(String status) throws DataInvalidException{
        try{
            this.status = Status.valueOf(status);
        }catch(IllegalArgumentException ex){
            throw new DataInvalidException("Status must only have following vlaues: "
                    + "PENDING, DONE or DELEGATED");
        }
        
    }
    
    @Override
    public String toString(){
        String dateString = simpleDate.format(this.dueDate);
        return String.format("%d: %s of %d difficulty is due on %s and status is %s", 
                                id, task, difficulty, dateString, status);
    }
}
