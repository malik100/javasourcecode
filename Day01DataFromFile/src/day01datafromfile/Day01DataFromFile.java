
package day01datafromfile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Day01DataFromFile {
    
    static final String DATA_FILE_NAME = "input.txt";
    static ArrayList<String> namesList = new ArrayList<>();
    static ArrayList<Double> numsList = new ArrayList<>();
    
    static void readDataFromFile(){
        try (Scanner fileInput = new Scanner(new File (DATA_FILE_NAME));){
            while(fileInput.hasNextLine()){
                String line = fileInput.nextLine();
                try{
                    double val = Double.parseDouble(line);
                    numsList.add(val);
                }catch(NumberFormatException n){
                    namesList.add(line);
                }
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
    }
    
    static void findDuplicateNames(){
        ArrayList<String> result = new ArrayList<>();
         for (int i = 0; i < namesList.size(); i++) {
            String n1 = namesList.get(i);
             for (int j = i+1 ; j < namesList.size(); j++) {
                 String n2 = namesList.get(j);
                 if(n1.equals(n2) && !result.contains(n2)){
                     result.add(n2);
                 }
             }
         }
         System.out.println("\nDuplicates: " + result);
         try(PrintWriter outputFile = new PrintWriter(new File("outputA.txt"))){
             for(String name : result){
                 outputFile.println(name);
             }
         }catch(IOException ee){
             System.out.println("Eooro writing to file");
         }
    }
    
    
    public static void main(String[] args) {
       
        readDataFromFile();
        Collections.sort(numsList);
        Collections.sort(namesList);
        
        System.out.println("Names: " + String.join(", ", namesList));
        
        double total = 0;
        for (int i = 0; i < namesList.size(); i++) {
            String current = namesList.get(i);
            total += current.length();
        }
        System.out.printf("%s %.2f", "The average length was", total/namesList.size());
        
        findDuplicateNames();
        
        System.out.println("\ndone");
        
        
    }
    
}
