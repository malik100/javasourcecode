
package javaapplication2;

import java.util.Scanner;


public class JavaApplication2 {

    
    public static void main(String[] args) {
        
//        int x = 5;
//        int y =1;
//        boolean validity = x >= 5;
//        System.out.println(validity);
//        boolean validityTwo = (validity)? true : false;
//        System.out.println(validityTwo);
    Scanner input = new Scanner(System.in);
System.out.println("Please enter the number of friends you have");
int numberOfFriends = input.nextInt();
input.nextLine(); // to clear any unwanted nextline tokens
String [] friendNames = new String[numberOfFriends];

    for (int i = 0; i < friendNames.length ; i++){
        System.out.println("Enter your friend's name");
        friendNames[i] = input.nextLine();
    }

System.out.println("Enter a search string (ex. rr)");
String searchString = input.nextLine();
int matchCount = 0;
String matchedName = "";
   
   for(int i = 0; i < friendNames.length; i++){
       if(friendNames[i].contains(searchString)){
           matchCount++;
           matchedName = friendNames[i];
           System.out.printf("Matched name found: %s \n", matchedName);
       }
   }

System.out.printf("The number of names that contain %s is %d", searchString, matchCount);
   

int longestLength = friendNames[0].length();
String longestName = friendNames[0];
int lengthSum = 0;
double lengthAverage = 0;

    for(int i = 0 ; i < friendNames.length; i++){
        if(friendNames[i].length() > longestLength){
            longestLength = friendNames[i].length();
            longestName = friendNames[i];
        }
        lengthSum += friendNames[i].length();
    }
lengthAverage = (double)lengthSum / friendNames.length;


System.out.printf("The longest name was %s, Total characters of all names were %d , Average length was %.2f",longestName, lengthSum, lengthAverage);
                     
                                          
}
    
          
          
         
    }


