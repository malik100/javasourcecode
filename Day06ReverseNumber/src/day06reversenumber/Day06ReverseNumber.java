/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day06reversenumber;

/**
 *
 * @author john
 */
public class Day06ReverseNumber {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int input = 1234567890;
        String inputAsString = "" + input;
        String reverseString = "";
        for (int i = inputAsString.length()-1; i >= 0; i--) {
            reverseString += inputAsString.charAt(i);
        }
        int reversedNumbers = Integer.parseInt(reverseString);
        System.out.println(reversedNumbers);
    }
    
}
