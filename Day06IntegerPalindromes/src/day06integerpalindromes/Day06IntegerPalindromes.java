/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day06integerpalindromes;

/**
 *
 * @author john
 */
public class Day06IntegerPalindromes {

    public static int reverse(int number){
        int result = 0;
        while(number > 0){
            int lastDigit = number % 10;
            result *= 10;
            result += lastDigit;
            number /= 10;
        }
        return (int)result;
    }
    public static boolean isPlaindrome(int number){
        
        String numberStr = "" + number;
        String reverseStr = "" + reverse(number);
        boolean palindrome = numberStr.equals(reverseStr);
        return palindrome;
        ////////////OR//// return (number == reverse(number);
    }
    public static void main(String[] args) {
        int value = 12345;
        int reverse = reverse(value);
        System.out.printf("value %d reversed is %d\n", value, reverse);
    }
    
}
