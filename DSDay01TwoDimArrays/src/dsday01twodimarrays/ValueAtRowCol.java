
package dsday01twodimarrays;


public class ValueAtRowCol {
    int value;
    int row;
    int col;
    public ValueAtRowCol(int value, int row, int col){
        this.value = value;
        this.row = row;
        this.col = col;
    }

    @Override
    public String toString() {
        return "ValueAtRowCol{" + "value=" + value + ", row=" + row + ", col=" + col + '}';
    }
    
    
}
