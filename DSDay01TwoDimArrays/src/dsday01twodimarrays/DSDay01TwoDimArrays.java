
package dsday01twodimarrays;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;


public class DSDay01TwoDimArrays {


    public static void main(String[] args) {
        //Initializing basics---------------------------------------------------
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter 2 numbers greater than 1");
        int height = input.nextInt();
        int width = input.nextInt();
        
        //creating and populating array-----------------------------------------
        int[][] arr = new int[height][width];
        PopulateArrRandoms(arr);
        
        //Printing Array--------------------------------------------------------
        Print2DArray(arr);
        /*
        //Sum All Numbers-------------------------------------------------------
        //System.out.println("Sum: " + SumArray(arr));
        
        //Sum Column------------------------------------------------------------
        int[] roww = SumRows(arr);
        //print row data--------------------------------------------------------
        System.out.println("====Rows Data====");
        for (int i = 0; i < roww.length; i++) {
            System.out.printf("%s has %d", i, roww[i]);
            System.out.println("");
        }
        //print Column data-----------------------------------------------------
        int[] colss = SumCols(arr);
        System.out.println("====Column Data====");
        for (int i = 0; i < colss.length; i++) {
            System.out.printf("%s has % d", i, colss[i]);
            System.out.println("");
        }
        
        //Standard deviation----------------------------------------------------
        int[][] nums = new int[][]{ {4, 8, 2}, {10, 5, 3}};
        System.out.println(GetStandardDev(arr));
        */
        //int[][] pairs = new int[][] {{34,67,24,25,9},{35,56,2,6,23}};
        int[][] pairs = new int[][]{{2, 3}, {8, 4}};
        PartOne(pairs);
    }
    
    
    
    public static void PartOne(int[][] arr) {
        List<Pair> pairList = new ArrayList<Pair>();
        for (int row = 0; row < arr.length; row++) {
            for (int col = 0; col < arr[row].length; col++) {
                int val = arr[row][col];
                PartTwo(new ValueAtRowCol(val, row, col), arr, pairList);
            }
        }
    }
     public static void PartTwo(ValueAtRowCol firstVal, int[][] arr, List<Pair> pairList) {
         int summedVal = 0;
        for (int row = 0; row < arr.length; row++) {
            for (int col = 0; col < arr[row].length; col++) {
                int newVal = arr[row][col];
                ValueAtRowCol secondVal = new ValueAtRowCol(newVal, row, col);
                summedVal = firstVal.value + secondVal.value;
                if(CheckPairDuplicate(pairList, firstVal, secondVal) == false){
                    continue;
                }
                if(isPrime(summedVal) == true){
                    System.out.println("========================");
                    System.out.printf("A prime value was found [%d] + [%d] = [%d]", 
                            firstVal.value, secondVal.value, summedVal );
                    System.out.println("========================");
                    pairList.add(new Pair(firstVal, secondVal));
                }
            }
        }
    }
     public static boolean CheckPairDuplicate(List<Pair> pairList, 
             ValueAtRowCol val1, ValueAtRowCol val2 ){
         for(Pair p : pairList){
             if((p.val1.col == val2.col && p.val1.row == val2.row) 
                     && (p.val2.col == val1.col && p.val2.row == val1.row)){
                 return false;
             }
         }
         
         return true;
     }
    
    //check for prime numbers---------------------------------------------------
    public static boolean isPrime(int num) {
        if (num == 1) return false; // by definition, 1 is not a prime number
        for (int i = 2; i <= num / 2; i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }
    
    
    
    //Generate randoms to poulate array-----------------------------------------
    public static void PopulateArrRandoms(int[][] arr) {
        for (int row = 0; row < arr.length; row++) {
            for (int col = 0; col < arr[row].length; col++) {
                arr[row][col] = (int)((Math.random() * (100 - -100 )) + -100 );
            }
        }
    }

    //Print 2D Array------------------------------------------------------------
    private static void Print2DArray(int[][] arr) {
         for (int row = 0; row < arr.length; row++) {
             for (int col = 0; col < arr[row].length; col++) {
                 System.out.printf("%s%d", col == 0 ? "" : ", " , arr[row][col]);
             }
             System.out.println("");
        }
    }

    //Sum up Array--------------------------------------------------------------
    public static int SumArray(int[][] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                sum += arr[i][j];
            }
        }
        return sum;
    }
    //Sum Rows------------------------------------------------------------------
    public static int[] SumRows(int[][] arr) {
        int[] rowSum = new int[arr.length];
        
        for (int row = 0; row < arr.length; row++) {
            for (int col = 0; col < arr[row].length; col++) {
                rowSum[row] += arr[row][col];
            }
        }
        return rowSum;
    }
    //Sum Cols------------------------------------------------------------------
    public static int[] SumCols(int[][] data) {
        /*
        int[] colSum = new int[arr[0].length];
        for (int row = 0; row < arr.length; row++) {
            for (int col = 0; col < arr[row].length; col++) {
                colSum[row] += arr[col][row];
            }
        }
        return colSum;
        */
        int[] colSums = new int[data[0].length];
        // NOTE: this only works for rectangular arrays
        for (int col = 0; col < data[0].length; col++) {
            int sum = 0;
            for (int row = 0; row < data.length; row++) {
                sum += data[row][col];
            }
            colSums[col] = sum;
        }
        return colSums;
    }
    //Get Mean(Average of Array)------------------------------------------------
    public static double GetStandardDev(int[][] arr) {
        double arrSize = (arr.length * arr[0].length);
        double mean = (SumArray(arr) / arrSize);
        System.out.println("Mean is : " + mean);
        double primaryDev = 0;
        for (int row = 0; row < arr.length; row++) {
            for (int col = 0; col < arr[row].length; col++) {
                double currentCalc = (double)arr[row][col] - mean;
                primaryDev += Math.pow( currentCalc , 2);
            }
        }
        System.out.println("Primary dev is : " + primaryDev);
        primaryDev /= arrSize;
        return Math.sqrt(primaryDev);
    }

    
    
}
