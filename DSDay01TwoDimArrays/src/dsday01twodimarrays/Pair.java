
package dsday01twodimarrays;


public class Pair {
    ValueAtRowCol val1;
    ValueAtRowCol val2;
    public Pair(ValueAtRowCol val1, ValueAtRowCol val2){
        this.val1 = val1;
        this.val2 = val2;
    }
}
