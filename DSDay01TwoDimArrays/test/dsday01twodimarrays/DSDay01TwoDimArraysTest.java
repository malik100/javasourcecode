/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsday01twodimarrays;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author john
 */
public class DSDay01TwoDimArraysTest {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    
    
    

    
   

    @Test
    public void testIsPrime() {
        int num = 6;
        boolean expResult = true;
        boolean result = DSDay01TwoDimArrays.isPrime(num);
        assertEquals(expResult, result);
    }

    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        DSDay01TwoDimArrays.main(args);
        fail("The test case is a prototype.");
    }

    @Test
    public void testPartOne() {
        System.out.println("PartOne");
        int[][] arr = null;
        DSDay01TwoDimArrays.PartOne(arr);
        fail("The test case is a prototype.");
    }

    @Test
    public void testPartTwo() {
        System.out.println("PartTwo");
        ValueAtRowCol firstVal = null;
        int[][] arr = null;
        List<Pair> pairList = null;
        DSDay01TwoDimArrays.PartTwo(firstVal, arr, pairList);
        fail("The test case is a prototype.");
    }

    @Test
    public void testCheckPairDuplicate() {
        System.out.println("CheckPairDuplicate");
        List<Pair> pairList = null;
        ValueAtRowCol val1 = null;
        ValueAtRowCol val2 = null;
        boolean expResult = false;
        boolean result = DSDay01TwoDimArrays.CheckPairDuplicate(pairList, val1, val2);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    @Test
    public void testPopulateArrRandoms() {
        System.out.println("PopulateArrRandoms");
        int[][] arr = null;
        DSDay01TwoDimArrays.PopulateArrRandoms(arr);
        fail("The test case is a prototype.");
    }

    @Test
    public void testSumArray() {
        System.out.println("SumArray");
        int[][] arr = {{-99, 2, 22},{15, -71, 5}};
        int expResult = -126;
        int result = DSDay01TwoDimArrays.SumArray(arr);
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    @Test
    public void testSumRows() {
        System.out.println("SumRows");
        int[][] arr = {{-99, 2, 22},{15, -71, 5}};
        int[] expResult = {-75, -51};
        int[] result = DSDay01TwoDimArrays.SumRows(arr);
        assertArrayEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    @Test
    public void testSumCols() {
        System.out.println("SumCols");
        int[][] arr = {{-99, 2, 22},{15, -71, 5}};
        int[] expResult = {-84, -69, 27};
        int[] result = DSDay01TwoDimArrays.SumCols(arr);
        assertArrayEquals(expResult, result);
        //fail("The test case is a prototype.");
    }

    @Test
    public void testGetStandardDev() {
        System.out.println("GetStandardDev");
        int[][] arr = {{-99, 2, 22},{15, -71, 5}};
        double expResult = 46.42915;
        double result = DSDay01TwoDimArrays.GetStandardDev(arr);
        assertEquals(expResult, result, 0.00001);
        //fail("The test case is a prototype.");
    }

    
    
}
