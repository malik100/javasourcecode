/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabasicsoop;

/**
 *
 * @author john
 */
public class Book implements Comparable<Book>{
    int pages;
    String title;
    String author;
    
    public Book(int pages, String author, String title){
        this.pages = pages;
        this.author = author;
        this.title = title;
    }
    
    public int compareTo(Book otherBook){
        if(this.pages != otherBook.pages){
            return this.pages - otherBook.pages;
        }
        if(!this.title.equals(otherBook.title)){
            return this.title.compareTo(otherBook.title);
        }
        return this.author.compareTo(otherBook.author);
    }
}
