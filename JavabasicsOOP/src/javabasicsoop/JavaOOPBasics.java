/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabasicsoop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

/**
 *
 * @author john
 */
public class JavaOOPBasics {
    public static void main(String[] args) {
        Book book100 = new Book(250, "John Smith", "Sample Book");
        Book book999 = new Book(80, "John White", "Another Sample Book");
        Book book20 = new Book(250, "Lisa Black", "Book On Trees");
        
        HashMap<Integer, String> h = new HashMap();
        h.put(1, "Montreal");
        h.put(2, "Laval");
        h.put(3, "St. Laurent");
        System.out.println(h.get(2));
    }
}
