/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day07occurenceofnumbers;

import java.util.Scanner;

/**
 *
 * @author john
 */
public class Day07OccurenceOfNumbers {

    static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) {
        int[] frequencies = new int[101];
        //ask user for value
        //increment frequencies of [n]
        int counter;
        while(true){
            System.out.println("Enter a number from 1-100");
            int n = input.nextInt();
            if(n == 0){
                break;
            }
            frequencies[n]++;
        }
        int placeMarker;
        for (int i = 0; i < frequencies.length; i++) {
            int freq = frequencies[i];
            if (freq == 0){
                continue;
            }
            System.out.printf("%d occurs %d time%s\n",i, freq, freq == 1 ? "" : "s" );
        }
        
        
    }
    
}
