package EmailAndSMS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class App {

	public static void main(String[] args){
		
		List<Message> messageList = new ArrayList<Message>();
		
		EmailMessage email = new EmailMessage(
				new EmailUser("Judy", "Foster", new Address("Main Street", 1), "ab@g.com"),
				new EmailUser("Betty", "Beans", new Address("Second Street", 2), "vr@g.com"),
				"this is one email12345");
		
		SmsMessage smsMessage = new SmsMessage(
				new SmsUser("Judy", "Foster", new Address("Main Street", 1), "1221231111"),
				new SmsUser("Betty", "Beans", new Address("Second Street", 2), "12323131111"),
				"this is one sms12345");
		
		messageList.add(email);  //adding to list
		messageList.add(smsMessage);
		
		SendSms smsSend = new SendSms();  //creating send classes
		SendEmail emailSend = new SendEmail();
		
		
		
		try {
			//Iterating through list and checking for instance of type before trying to send sms or email
			for (int i = 0; i < messageList.size(); i++) {
				if(messageList.get(i) instanceof EmailMessage) {
					//downcasting message to emailmessage
					if(emailSend.ValidateEmail((EmailUser)messageList.get(i).getReciever(), (EmailUser)messageList.get(i).getSender(), messageList.get(i).getBody())) {
						emailSend.sendMessage(email);
					}else {
						System.out.println("Sorry your email message failed the validation test and was not sent");
					}
					
					
				}
				if(messageList.get(i) instanceof SmsMessage) {
					//downcasting message to smsmessage
					if(smsSend.ValidateSms((SmsUser)messageList.get(i).getReciever(), (SmsUser)messageList.get(i).getSender(), messageList.get(i).getBody())) {
						smsSend.sendMessage(smsMessage);
					}else {
						System.out.println("Sorry your sms message failed the validation test and was not sent");
					}
					
				}
			}
		} catch (IOException e) {				//Also catches any IO exceptions coming from inside classes
			System.out.println("There was an input or output error please try again after verifying your information");
			System.exit(1);
		} catch (IllegalArgumentException f) {	//catches the illegal argument exceptions coming from inside classes
			System.out.println("The information you entered was invalid please verify your information and try again");
			System.exit(1);
		}
	

		
	}

}
