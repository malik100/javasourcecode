package EmailAndSMS;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class SendSms implements ISendInfo{

	@Override
	public boolean validateMessage(User sender, User reciever, String body) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean ValidateSms(SmsUser sender, SmsUser reciever, String body) {
		///checking for 10 characters in phone number
		if(sender.getPhoneNumber().length() < 10) {
			throw new IllegalArgumentException("Phone number was less than 10 characters please try again with correct phone number");
		}
		//checking if sms body is empty
		if(body.equals("")) {
			throw new IllegalArgumentException("No sms message was entered please try agin");
		}
		//checking if sms body greater than 140 characters
		if(body.length() > 160) {
			throw new IllegalArgumentException("your message was longer than 140 characters");
		}
		if(body.contains("&") || body.contains("#") || body.contains("@")) {
			throw new IllegalArgumentException("Email contains illegal characters & or # or @ please try agin without those characters");
		}
		return true;
	}
	
	@Override
	public void sendMessage(Message message) throws IOException {
		FileWriter fileWriter = new FileWriter("sms.txt");
		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
		bufferedWriter.write(message.getBody());
		bufferedWriter.close();
		fileWriter.close();
		
	}


}
