package EmailAndSMS;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class SendEmail implements ISendInfo{
	
	@Override
	public boolean validateMessage(User sender, User reciever, String body) {
		return false;
	}
	
	public boolean ValidateEmail(EmailUser sender, EmailUser reciever, String body) {
		//checking if email message is blank
		if(body.equals("")) {
			throw new IllegalArgumentException("Email body contains no message please try again");
		}
		//checking for ^ ! *
		if(body.contains("!") || body.contains("*") || body.contains("^")) {
			throw new IllegalArgumentException("Email contains illegal characters ! or * or ^ please try agin without those characters");
		}
		int atCountSender = 0;
		int dotCountSender = 0;
		int atCountReciever = 0;
		int dotCountReciever = 0;
		for (int i = 0; i < reciever.getEmailAddress().length(); i++) {
			if(reciever.getEmailAddress().charAt(i) == '@'){
				atCountReciever++;
			};
			if(reciever.getEmailAddress().charAt(i) == '.'){
				dotCountReciever++;
			};
		}
		for (int i = 0; i < sender.getEmailAddress().length(); i++) {
			if(sender.getEmailAddress().charAt(i) == '@'){
				atCountSender++;
			};
			if(sender.getEmailAddress().charAt(i) == '.'){
				dotCountSender++;
			};
		}
		if(dotCountReciever > 1 || atCountReciever > 1 ) {
			System.out.println("too many @ or .");
			return false;
		}
		if(dotCountSender > 1 || atCountSender > 1 ) {
			System.out.println("too many @ or .");
			return false;
		}
		///checking for @ & .
		if((sender.getEmailAddress().contains("@") && sender.getEmailAddress().contains(".")) &&
			(reciever.getEmailAddress().contains("@") && reciever.getEmailAddress().contains("."))) {
			return true;
		}
		return true;
	}

	@Override
	public void sendMessage(Message message) throws IOException {
		FileWriter fileWriter = new FileWriter("email.txt");
		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
		bufferedWriter.write(message.getBody());
		bufferedWriter.close();
		fileWriter.close();
		
	}

	

}
