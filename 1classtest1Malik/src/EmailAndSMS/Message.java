package EmailAndSMS;

public class Message {
	private User reciever;
	private User sender;
	private String body;
	public Message(User reciever, User sender, String body) {
		super();
		this.reciever = reciever;
		this.sender = sender;
		this.body = body;
	}
	public User getReciever() {
		return reciever;
	}
	public void setReciever(User reciever) {
		this.reciever = reciever;
	}
	public User getSender() {
		return sender;
	}
	public void setSender(User sender) {
		this.sender = sender;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	
}
