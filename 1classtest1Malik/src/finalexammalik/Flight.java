/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalexammalik;

import java.util.Date;
import javax.swing.JOptionPane;


public class Flight {
	int id;
	Date onDate;
	String fromCode, toCode;
	Type  type;
	enum Type { DOMESTIC, INTERNATIONAL, PRIVATE };
	int passengers;

    public Flight(int id, Date onDate, String fromCode, String toCode, finalexammalik.Flight.Type type, int passengers){
        this.id = id;
        this.onDate = onDate;
        this.fromCode = fromCode;
        this.toCode = toCode;
        this.type = type;
        this.passengers = passengers;
    }
        
//Setters-----------------------------------------------------------------------
    public void setId(int id) throws DataInvalidException {
        if(id < 0){
            throw new DataInvalidException("ID must not be a negative number");
        }
        this.id = id;
    }

    public void setOnDate(Date onDate) {
        this.onDate = onDate;
    }

    public void setFromCode(String fromCode) {
        this.fromCode = fromCode;
    }

    public void setToCode(String toCode) {
        this.toCode = toCode;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setPassengers(int passengers) {
        this.passengers = passengers;
    }
//TOSTRING----------------------------------------------------------------------

    @Override
    public String toString() {
        String dateStr = FinalExamMalik.simpleDate.format(onDate);
        return String.format("%s: %s from %s to %s on %s with %d passenegers", 
                id, type, fromCode, toCode, dateStr, passengers);
    }
    
        
}
