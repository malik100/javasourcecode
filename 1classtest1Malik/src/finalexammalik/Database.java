/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalexammalik;

import finalexammalik.Flight.Type;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;


public class Database {
    private static final String dbURL = "jdbc:mysql://localhost:3306/finaldb";
    private static final String username = "root";
    private static final String password = "Y9}-teQ!5";

    private Connection conn;

    public Database() throws SQLException {
        conn = DriverManager.getConnection(dbURL, username, password);
    }

    public ArrayList<Flight> getAllFlights() throws SQLException, DataInvalidException {
        ArrayList<Flight> list = new ArrayList<>();
        String sql = "SELECT * from flights";
        PreparedStatement statement = conn.prepareStatement(sql);
        try (ResultSet result = statement.executeQuery(sql)) {
            while (result.next()) {
                int id = result.getInt("id");
                Date date = result.getDate("onDay");
                String fromCode = result.getString("fromCode");
                String toCode = result.getString("toCode");
                Type type = Type.valueOf(result.getString("type"));     //Illegal Argument ex possible
                int passengers = result.getInt("passengers");
                list.add(new Flight(id, date, fromCode, toCode, type, passengers));     //DataInvalid EX
            }
        }catch(IllegalArgumentException ex){  //chained to Datainvalid ex
        throw new DataInvalidException("Enum value incorreect " + ex.getMessage());
    }
        return list;
    }

    public Flight getFlightById(int id) throws SQLException, DataInvalidException {
        PreparedStatement stmtSelect = conn.prepareStatement("SELECT * from flights where id=?");
        stmtSelect.setInt(1, id);
        try (ResultSet resultSet = stmtSelect.executeQuery()) {
            if (resultSet.next()) {
                Date date = resultSet.getDate("onDay");
                String fromCode = resultSet.getString("fromCode");
                String toCode = resultSet.getString("toCode");
                Type type = Type.valueOf(resultSet.getString("type"));     //Illegal Argument ex possible
                int passengers = resultSet.getInt("passengers");
                return new Flight(id, date, fromCode, toCode, type, passengers);      //DataInvalid EX
            } else {
                throw new SQLException("Record not found");
            }
        }catch(IllegalArgumentException ex){ //chained to Datainvalid ex
        throw new DataInvalidException("Enum value incorreect " + ex.getMessage());
    }
    }

    public void addFlight(Flight flight) throws SQLException {
        String sql = ("insert into flights values (null, ?, ?, ?, ?, ?)");
        PreparedStatement statement = conn.prepareStatement(sql);
        java.sql.Date sqlDate = new java.sql.Date(flight.onDate.getTime()); //conversion
        System.out.println(sqlDate);
        statement.setDate(1, sqlDate);
        statement.setString(2, flight.fromCode);
        statement.setString(3, flight.toCode);
        statement.setString(4, flight.type.toString());
        statement.setInt(5, flight.passengers);
        statement.executeUpdate();
        System.out.println("Record inserted");
    }
 
    
    public void modifyFlight(Flight flight) throws SQLException{
        String sql = "update flights set onDay=?, fromCode=?, toCode=?, "
                + "type=?, passengers=? where id=?";
        PreparedStatement statement = conn.prepareStatement(sql);
        java.sql.Date sqlDate = new java.sql.Date(flight.onDate.getTime()); //conversion
        statement.setDate(1, sqlDate);
        statement.setString(2, flight.fromCode);
        statement.setString(3, flight.toCode);
        statement.setString(4, flight.type.toString());
        statement.setInt(5, flight.passengers);
        statement.setInt(6, flight.id);
        statement.executeUpdate();
        System.out.println("Record updated id =" + flight.id);
    }
    
    public void deleteFlight(Flight flight) throws SQLException{
        String sql = "delete from flights where id=?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, flight.id);
        statement.executeUpdate();
        System.out.println("Record deleted id =" + flight.id);
    }

}
