/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day05sqrt;

/**
 *
 * @author john
 */
public class Day05Sqrt {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.printf("%5s     %7s \n", "Number", "SquareRoot");
        for (int i = 0; i <= 20; i += 2 ) {
            System.out.printf("%-7d     %7.4f \n", i, Math.sqrt(i));
        }
   
        
    }
    
}
