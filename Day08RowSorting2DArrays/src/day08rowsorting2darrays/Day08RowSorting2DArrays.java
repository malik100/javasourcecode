package day08rowsorting2darrays;

import java.util.Arrays;

public class Day08RowSorting2DArrays {

    public static void main(String[] args) {
        int[][] arr = {
            {3, 5, 1},
            {1, 3, 2},
            {6, 4, 1},};
        for (int k = 0; k < 10; k++) {
            for (int row = 0; row < arr.length; row++) {
                for (int i = 0; i < arr[row].length - 1; i++) {
                    if (arr[row][i] > arr[row][i + 1]) {
                        int temp = arr[row][i + 1];
                        arr[row][i + 1] = arr[row][i];
                        arr[row][i] = temp;
                    }
                }
            }
        }

        System.out.println(Arrays.deepToString(arr));
    }

}
