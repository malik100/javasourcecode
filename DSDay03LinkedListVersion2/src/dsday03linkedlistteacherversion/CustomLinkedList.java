
package dsday03linkedlistteacherversion;


public class CustomLinkedList {
    private class Container {
		Container next;
		String value;

        public Container(Container next, String value) {
            this.next = next;
            this.value = value;
        }

        @Override
        public String toString() {
            return "Container{" + "next=" + next + ", value=" + value + '}';
        }
                
	}
	private Container start, end;
	private int size;

	public void add(String value) {
            Container newCont = new Container(null, value);
        if(size == 0){ //first entry into list
            start = newCont;
            end = newCont;
            size = 1;
        }else{ //append item onto end
            end.next = newCont;
            end = newCont;
            size++;
        }
        }
	public String get(int index) {
            Container current = start;
            if(index == 0){
                return start.value;
            }
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            return current.value;
        }
        
	public void insertValueAtIndex(String value, int index) {
            Container before = start;
            if(index == size || size == 0){
                add(value);
                return;
            }
            if(index == 0){
                Container newCont = new Container(start, value);
                start = newCont;
                return;
            }
            for (int i = 0; i < index-1; i++) {
                before = before.next;
            }
            Container newCont = new Container(before.next ,value);
            before.next = newCont;
            size++;
        }
        
	public void deleteByIndex(int index) {
            if(index == 0 || size == 1){ //first item
                start = start.next;
                size--;
                return;
            }
            Container previous = start;
            for (int i = 0; i < index - 1; i++) {
                previous = previous.next;
            }
            if(index == size - 1){
                end = previous;
            }
            previous.next = previous.next.next;
            size--;
        }
        
	public boolean deleteByValue(String value) {
            Container current = start;
            int index = -1;
            for (int i = 0; i < size; i++) { //look for match to delete
                if(current.value.equals(value)){
                    index = i;
                }
                current = current.next;
            }
            if(index == -1){return false;}//no match
            if(index == 0){
                start = start.next;
                size--;
                return true;
            }
            Container before = start;
            for (int i = 0; i < index-1; i++) {
                before = before.next;
            }
            if(before.next == end){
                end = before;
                before.next = null;
                size--;
                return true;
            }
            before.next = before.next.next;
            return true;
        }
	public int getSize() {
            return size;
        }
	
	@Override
	public String toString() {
        StringBuilder sb = new StringBuilder("[");
            for (Container current = start;  current != null; current = current.next) {
                sb.append(current == start ? current.value : "," + current.value );
            }
        sb.append("]");
        return sb.toString();
        }

	public String[] toArray() {
            String[] arr = new String[size];
            Container current = start;
            for (int i = 0; i < size; i++) {
                arr[i] = current.value;
                current = current.next;
            }
            return arr;
        }

}
