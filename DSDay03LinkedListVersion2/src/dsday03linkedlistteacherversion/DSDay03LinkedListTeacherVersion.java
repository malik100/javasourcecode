
package dsday03linkedlistteacherversion;


public class DSDay03LinkedListTeacherVersion {

    
    public static void main(String[] args) {
        CustomLinkedList list = new CustomLinkedList();
        list.add("first");
        list.add("second");
        list.add("third");
        list.add("fourth");
        list.add("fifth");
        System.out.println(list.deleteByValue("third"));
        
        System.out.println(list);
        
    }
    
}
