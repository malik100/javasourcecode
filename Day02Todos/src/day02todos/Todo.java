/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day02todos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
    
enum TaskStatus{
    Pending, Done;
}
/**
 *
 * @author john
 */
public class Todo {
    private String task;
    private Date dueDate;
    private int hoursOfWork;
    private TaskStatus status;
    private static int instanceCount;
    
    public Todo(String task, Date date, int hours){
        setTask(task);
        setDueDate(date);
        setHoursOfWork(hours);
        instanceCount++;
    }
    public Todo(String dataLine) throws ParseException{
        String data[] = dataLine.split(";");
        for (int i = 0; i < data.length; i++) {
            System.out.println(data[i]);
        }
        if (data.length != 3) {
            throw new IllegalArgumentException("Invalid number of items in line");
        }
        setTask(data[0]);                                       //IllegalArgumentException possible
        setDueDate(Globals.dateFormatScreen.parse(data[1]));    //ParseException possible
        setHoursOfWork(Integer.parseInt(data[2]));                 //NumberFormatException possible
        
        instanceCount++;
    }
    
    private static final SimpleDateFormat dateFormatFile;
    static { // static initializer
        dateFormatFile = new SimpleDateFormat("yyyy-MM-dd");
        dateFormatFile.setLenient(false);
    }
    public String toDataString(){
        //return this.task + ";" + this.dueDate + ";" + this.hoursOfWork;
        String dueDateStr = dateFormatFile.format(dueDate);
        return String.format("%s;%s;%d", task, dueDateStr, hoursOfWork);
    }
    
    @Override
    public String toString(){
        return this.task + " Due on " + this.dueDate + " will take " + this.hoursOfWork + " hours.";
    }

    public String getTask(){
        return this.task;
    }
    
    
    
    
    
    public void setTask(String task){   //IllegalArgumentException Possible
        if(task.length() < 2 || task.length() > 50 || task.contains(";") || task.contains("|") || task.contains("`")){
            throw new IllegalArgumentException("Task was too long or short or contained illegal characters");
        }
        this.task = task;
    }
    
    public Date getDueDate(){
        return this.dueDate;
    }
    
    public void setDueDate(Date dueDate){
        //Logic to Add
        this.dueDate = dueDate;
    }
    
    public int getHoursOFWork(){
        return this.hoursOfWork;
    }
    public void setHoursOfWork(int hours){
        if(hours < 0){                              //IllegalArgumentException possible
            throw new IllegalArgumentException("Hours must be 0 or greater");
        }
        this.hoursOfWork = hours;
    }
    public static int getInstanceCount(){
        return instanceCount;
    }
    
}
