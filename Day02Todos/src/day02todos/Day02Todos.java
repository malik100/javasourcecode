package day02todos;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Day02Todos {

    static ArrayList<Todo> todoList = new ArrayList<>();
    static Scanner input = new Scanner(System.in);
    static final String TODO_LIST_DIRECTORY = "todo.txt";
    public static void main(String[] args) {
        loadDataFromFile(TODO_LIST_DIRECTORY);
        while (true) {
            try {
                System.out.println("---------------------------------------------------");
                System.out.println("Current instance count: " + Todo.getInstanceCount());
                int choice = getMenuChoice();
                switch (choice) {
                    case 1:
                        System.out.println("[You chose to add a task]");
                        addToDo();
                        break;
                    case 2:
                        System.out.println("[You chose to list all tasks]");
                        listAllTodosNumbered();
                        break;
                    case 3:
                        System.out.println("[You chose to delete a task]");
                        deleteTodo();
                        break;
                    case 4:
                        System.out.println("[You chose to modify a task]");
                        modifyTodo();
                        break;
                    case 0:
                        System.out.println("[You chose to exit]");
                        return;
                    default:
                        System.out.println("[Invalid choice please enter 1, 2, 3, 4 or 0 to exit]");
                        break;
                }
            } catch (InputMismatchException i) {
                System.out.println("Wrong character entered please try again");
                input.nextLine();
            }
            saveDataToFile("todoNEW.txt");
        }
    }

    static void loadDataFromFile(String path) {
        try (Scanner reader = new Scanner(new File(path))) {
            while (reader.hasNextLine()) {
                String dataLine = reader.nextLine();
                System.out.println("Dataline is " + dataLine);
                todoList.add(new Todo(dataLine));
            }

        } catch (IOException io) {
            System.out.println("An IO error occured");
        } catch (NumberFormatException n) {
            System.out.println("Error " + n.getMessage());
        } catch (IllegalArgumentException il) {
            System.out.println("Error " + il.getMessage());
        } catch (ParseException p) {
            System.out.println("Error " + p.getMessage());
        }
    }

    static void saveDataToFile(String path) {
        try(PrintWriter out = new PrintWriter(new File (path))){
            for(Todo todo : todoList){
                out.println(todo.toDataString());
            }
        }catch(IOException i){
            System.out.println("IO error during process: " + i.getMessage());
        }
    }

    private static int getMenuChoice() {
        System.out.print("Please make a choice [0-4]:\n"
                + "1. Add a todo\n"
                + "2. List all todos (numbered)\n"
                + "3. Delete a todo\n"
                + "4. Modify a todo\n"
                + "0. Exit\n"
                + "Your choice is: ");
        try {
            int choice = input.nextInt();
            input.nextLine();
            return choice;
        } catch (InputMismatchException e) {
            System.out.println("wrong selection please try again");
            input.nextLine();
        }
        return 5;
    }

    private static void addToDo() {
        try {
            System.out.println("Enter a task description");
            String task = input.nextLine();
            System.out.println("Enter due date yyyy/mm/dd");
            String dueDateStr = input.nextLine();                       //IllegalArgumentException possible
            Date dueDate = Globals.dateFormatScreen.parse(dueDateStr);  //ParseException possible
            System.out.println("Enter hours of work needed");
            int hours = input.nextInt();                                //NumberFormatException possible
            Todo todo = new Todo(task, dueDate, hours);
            todoList.add(todo);
        } catch (NumberFormatException | ParseException ex) {
            System.out.println("Error parsing " + ex.getMessage());
        } catch (IllegalArgumentException ie) {
            System.out.println("Error: " + ie.getMessage());
        }

    }

    private static void listAllTodosNumbered() {
        if (todoList.isEmpty()) {
            System.out.println("[[[No todos found]]]");
            return;
        }
        for (int i = 0; i < todoList.size(); i++) {
            System.out.println("#" + (i + 1) + ": " + todoList.get(i));
        }
    }

    private static void deleteTodo() {
        try {
            System.out.println("Which todo would you like to delete? here are the available options");
            if (todoList.isEmpty()) {
                System.out.println("[[No items available for deletion]]");
                return;
            }
            for (int i = 0; i < todoList.size(); i++) {
                System.out.println("#" + (i + 1) + " " + todoList.get(i));
            }
            int toDelete = input.nextInt(); //InputMismatchException possible
            if (toDelete > todoList.size() || toDelete <= 0) {
                System.out.println("Incorrect option picked returning to main menu");
                return;
            }
            toDelete--;
            todoList.remove(toDelete);
            System.out.println("Deletion successful");
        } catch (InputMismatchException i) {
            System.out.println("Error wrone data type :" + i.getMessage());
        }
    }

    private static void modifyTodo() {
        try {
            System.out.println("Which todo would you like to modify? here are your options");
            if (todoList.isEmpty()) {
                System.out.println("[[No Current Taske Available To Modify]]");
                return;
            }
            for (int i = 0; i < todoList.size(); i++) {
                System.out.println("#" + (i + 1) + ": " + todoList.get(i).getTask());
            }
            int modChoice = input.nextInt();
            System.out.println("MODCHOICE WAS: " + modChoice);
            if (modChoice > todoList.size() || modChoice <= 0) {
                System.out.println("Error that option doesn't exist returnning to main menu");
                return;
            }
            modChoice--;
            System.out.println("NEW MODCHOICE: " + modChoice);
            System.out.println("Enter new task description");
            input.nextLine();
            String newTask = input.nextLine();
            System.out.println("New task was " + newTask);
            System.out.println(todoList.get(modChoice));
            todoList.get(modChoice).setTask(newTask);   // IllegalArgumentException possible
            System.out.println("Enter new due date yyyy/mm/dd");
            String newDateStr = input.nextLine();
            todoList.get(modChoice).setDueDate(Globals.dateFormatScreen.parse(newDateStr)); //ParseException possible
            System.out.println("Enter ne hours of work needed");
            int newHours = input.nextInt();                     //InputMismatchException possible
            todoList.get(modChoice).setHoursOfWork(newHours);
        } catch (NumberFormatException n) {
            System.out.println("Error: " + n.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println("Error " + e.getMessage());
        } catch (ParseException p) {
            System.out.println("Error Parsing Date " + p.getMessage());
        } catch (InputMismatchException im) {
            System.out.println("Error " + im.getMessage());
            input.nextLine();
        }

    }

}
