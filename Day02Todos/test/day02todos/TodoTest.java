/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day02todos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author john
 */
public class TodoTest {
    
    

//    @Test
//    public void testToDataString() throws ParseException {
//        System.out.println("toDataString");
//        Date date= new SimpleDateFormat("dd/MM/yyyy").parse("20/10/2001");
//        Todo instance = new Todo("Run this test", date, 3);
//        String expResult = "Run this test;2001-10-20;3";
//        String result = instance.toDataString();
//        assertEquals(expResult, result);
//        //fail("The test case is a prototype.");
//    }
//
//    @Test
//    public void testToString() {
//        System.out.println("toString");
//        Todo instance = null;
//        String expResult = "";
//        String result = instance.toString();
//        assertEquals(expResult, result);
//        fail("The test case is a prototype.");
//    }
//
//    @Test
//    public void testGetTask() {
//        System.out.println("getTask");
//        Todo instance = null;
//        String expResult = "";
//        String result = instance.getTask();
//        assertEquals(expResult, result);
//        fail("The test case is a prototype.");
//    }
//
//    @Test
//    public void testSetTask() {
//        System.out.println("setTask");
//        String task = "";
//        Todo instance = null;
//        instance.setTask(task);
//        fail("The test case is a prototype.");
//    }
//
//    @Test
//    public void testGetDueDate() {
//        System.out.println("getDueDate");
//        Todo instance = null;
//        Date expResult = null;
//        Date result = instance.getDueDate();
//        assertEquals(expResult, result);
//        fail("The test case is a prototype.");
//    }
//
//    @Test
//    public void testSetDueDate() {
//        System.out.println("setDueDate");
//        Date dueDate = null;
//        Todo instance = null;
//        instance.setDueDate(dueDate);
//        fail("The test case is a prototype.");
//    }
//
//    @Test
//    public void testGetHoursOFWork() {
//        System.out.println("getHoursOFWork");
//        Todo instance = null;
//        int expResult = 0;
//        int result = instance.getHoursOFWork();
//        assertEquals(expResult, result);
//        fail("The test case is a prototype.");
//    }
//
//    @Test
//    public void testSetHoursOfWork() {
//        System.out.println("setHoursOfWork");
//        int hours = 0;
//        Todo instance = null;
//        instance.setHoursOfWork(hours);
//        fail("The test case is a prototype.");
//    }
//
//    @Test
//    public void testGetInstanceCount() {
//        System.out.println("getInstanceCount");
//        int expResult = 0;
//        int result = Todo.getInstanceCount();
//        assertEquals(expResult, result);
//        fail("The test case is a prototype.");
//    }
    
    @Test
    public void testDataLineConstructor() throws ParseException{
        System.out.println("Todo(String dataLine");
        Todo instance = new Todo("Run this test;2001-10-20;3"); 
        assertEquals("Run this test", instance.getTask());
        Date date= new SimpleDateFormat("dd/MM/yyyy").parse("20/10/2001");
        assertEquals(date, instance.getDueDate());
        assertEquals(3, instance.getHoursOFWork());
    }
    public void testDataLineConstructorZZZ() throws ParseException{
        System.out.println("Todo(String dataLine");
        Todo instance = new Todo("Run this test;2001-500-20;3"); 
        assertEquals("Run this test", instance.getTask());
        Date date= new SimpleDateFormat("dd/MM/yyyy").parse("20/10/2001");
        assertEquals(date, instance.getDueDate());
        assertEquals(3, instance.getHoursOFWork());
    }
}
