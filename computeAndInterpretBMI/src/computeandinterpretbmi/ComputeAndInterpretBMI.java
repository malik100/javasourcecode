/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package computeandinterpretbmi;

import java.util.Scanner;

/**
 *
 * @author john
 */
public class ComputeAndInterpretBMI {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double pounds;
        double feet;
        double inches;
        double BMI;
        System.out.println("Enter your weight in pounds: ");
        pounds = input.nextDouble();
        System.out.println("Enter height in inches: ");
        inches = input.nextDouble();
        if (pounds < 0 || inches < 0){
            System.out.println("Cant enter negative value");
            System.exit(1);
        }
        feet = inches / 12;
        BMI = (pounds / Math.pow(inches, 2)) * 703;
        System.out.println("Youre BMI is " + BMI);
        if (BMI < 15){
            System.out.println("You are very severely underweight");
        }
        else if (BMI < 16){
            System.out.println("You are severely underweight");
        }
        else if (BMI < 18.5){
            System.out.println("You are underweight");
        }
        else if (BMI < 25){
            System.out.println("You are normal weigtht");
        }
        else if (BMI < 30){
            System.out.println("You are overweigtht");
        }
        else if (BMI < 35){
            System.out.println("You are moderately obeset");
        }
        else if (BMI < 40){
            System.out.println("You are severely obese");
        }
        else{
            System.out.println("You are very severely obese");
        }
        
    }
    
}
