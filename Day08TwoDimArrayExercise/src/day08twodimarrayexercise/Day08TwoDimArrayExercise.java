
package day08twodimarrayexercise;

import java.util.Arrays;


public class Day08TwoDimArrayExercise {

   
    public static void main(String[] args) {
        int[][] arrOne ={
                {20,50,10,20},
                {60,99,85,33},
                {27,7,12,54},
                };
        int[][] arrTwo ={
                {20,50,10,20},
                {60,99,85,33},
                {27,7,12,54},
                };
        
//        for (int row = 0; row < arrOne.length; row++) {
//            for (int col = 0; col < arrOne[row].length; col++) {
//                arrOne[row][col]= (int)(Math.random() * 100);
//            }
//        }
//        for (int row = 0; row < arrTwo.length; row++) {
//            for (int col = 0; col < arrTwo[row].length; col++) {
//                arrTwo[row][col]= (int)(Math.random() * 100);
//           }
//        }
        double avgOne = 0;
        double avgTwo = 0;
        double count= 0;
        int largest = Integer.MIN_VALUE;
        for (int row = 0; row < arrOne.length; row++) {
            for (int col = 0; col < arrOne[row].length; col++) {
                avgOne += arrOne[row][col];
                System.out.print(arrOne[row][col] + ", ");
                count++;
                if(arrOne[row][col] > largest){
                    largest = arrOne[row][col];
                }
            }
            System.out.println("");
        }
        for (int row = 0; row < arrTwo.length; row++) {
            for (int col = 0; col < arrTwo[row].length; col++) {
                avgTwo += arrTwo[row][col];
                System.out.print(arrTwo[row][col] + ", ");
            }
            System.out.println("");
        }
        avgOne /= count;
        System.out.println("AverageOne: " + avgOne);
        System.out.println("Largest Number: " + largest);
    }
    
}
