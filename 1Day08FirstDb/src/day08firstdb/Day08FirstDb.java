package day08firstdb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.*;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Day08FirstDb {

    public static void main(String[] args) {
        String dbURL = "removed for security";
        String username = "removed for security";
        String password = "removed for security";

        Random random = new Random();
        Connection conn = null;
        //open DataBase connection
        try {
            conn = DriverManager.getConnection(dbURL, username, password);
            System.out.println("Connected");
//            if (conn != null) {
//                System.out.println("Connected");
//            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.exit(1);
        }
        //insert

        try {
            String sql = ("insert into people values (null, ?, ?)");
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, "bill" + random.nextInt(30));
            statement.setString(2, "" + random.nextInt(100));
            statement.executeUpdate();
            System.out.println("Record inserted");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        //select
        try{
            String sql = "SELECT * from people";
            PreparedStatement statement = conn.prepareStatement(sql);
            try(ResultSet result = statement.executeQuery(sql)){
                while(result.next()){
                    int id = result.getInt("id");
                    String name = result.getString("name");
                    int age = result.getInt("age");
                    System.out.printf("%d: %s is %d y/o\n", id, name, age);
                }
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }
        //update
        try{
            int id = random.nextInt(10);
            int newAge = random.nextInt(100) + 100;
            String sql = "update people set age=? where id=?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, newAge);
            statement.setInt(2, id);
            statement.executeUpdate();
            System.out.println("Record updated id =" + id);
        }catch(SQLException ex){
            ex.printStackTrace();
        }
        //delete
        try{
            int id = random.nextInt(10);
            String sql = "DELETE from people where id=?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            statement.executeUpdate();
            System.out.println("Record deleted id=" + id);
        }catch(SQLException ex){
            ex.printStackTrace();
        }

    }

}
