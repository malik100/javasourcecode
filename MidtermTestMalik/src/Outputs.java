
//STUDENT: MALIK ZAFAR

public class Outputs {

	public static void main(String[] args) {
		//outputs for Calculator with carpet and floor
		System.out.println("CARPETS------------");
		Carpet carpet = new Carpet(3.5);
		Floor floor = new Floor (2.75, 4.0);
		Calculator calculator = new Calculator(floor, carpet);
		System.out.println("total= " + calculator.getTotalCost());
	    carpet = new Carpet(1.5);
		floor = new Floor (5.4, 4.5);
		calculator = new Calculator(floor, carpet);
		System.out.println("total= " + calculator.getTotalCost());
		System.out.println("-------------------");
		
		///OUTPUTS FOR BANK
		System.out.println("BANKING------------");  //Outputs for bank
		BankA bankA = new BankA();
		bankA.setBalance(100);
		System.out.println("Balance of bankA is: " + bankA.getBalance());
		BankB bankB = new BankB();
		bankB.setBalance(150);
		System.out.println("Balance of bankB is: " + bankB.getBalance());
		BankC bankC = new BankC();
		bankC.setBalance(200);
		System.out.println("Balance of bankC is: " + bankC.getBalance());
		System.out.println("-------------------");
		
		//OUTPUTS FOR PC
		System.out.println("COMPUTER----------------");
		Dimensions dimensions = new Dimensions(20, 20, 5);
		Case theCase = new Case("220B", "Dell", "240", dimensions);
		
		Monitor theMonitor = new Monitor("27inch Beast", "Acer", 27,new Resolution(2540, 1440));
		
		Motherboard theMotherboard = new Motherboard("BJ-200", "ASUS", 4, 6, "v2.44");
		
		PC thePC = new PC(theCase, theMonitor, theMotherboard);
		
		thePC.getTheCase().pressPowerButton(); ///POWER BUTTON PRESS
		
		thePC.getTheMotherboard().loadMotherBoard();
		thePC.getTheMonitor().turnOnScreen();
		System.out.println();
		thePC.startupSequence();
	}

}
