
public class BankA extends Bank{            //CHILD CLASS WITH OVERRIDEN METHOD BUT SAME FIELD AS PARENT
	private double balance;
	
	public void setBalance(double balance) { ///USED TO SET BALANCE IN THE CLASS
		this.balance = balance;
	}

	@Override
	public double getBalance() {              //OVERRIDEN METHOD TO GET BALANCE
		return balance;
	}

}
