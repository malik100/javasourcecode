/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03pyramid;

/**
 *
 * @author john
 */
public class Day03Pyramid {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int input = 6;
        for (int y = 0; y < input; y++) {
            for (int j = 0; j < 10; j++) {
                for(int x = 0; x < input; x++){
                if(y >= input - x){
                    System.out.print(" # ");
                }
                else{
                    System.out.print("   ");
                }
            }
            }
            for (int k = 0; k < input; k++) {
                if(y >= k){
                    System.out.print(" # ");
                }
                else{
                    System.out.print("   ");
                }
            }
            System.out.println();
        }
    }
    
}
