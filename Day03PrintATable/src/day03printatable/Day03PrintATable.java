/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03printatable;

/**
 *
 * @author john
 */
public class Day03PrintATable {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        double j =2;
        System.out.printf("%-5s %-5s %-5s \n", "a", "b", "pow(a,b)");
        for (double i = 1; i <= 5; i++){
            double answer = Math.pow(i, j);
            System.out.printf("%-5.0f %-5.0f %-5.0f \n", i, j, answer);
            j++;
        }
        
    }
    
}
