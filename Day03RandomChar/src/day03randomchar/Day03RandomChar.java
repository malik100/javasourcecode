/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03randomchar;

import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author john
 */
public class Day03RandomChar {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        char[] alphabet = new char[26];
        Random numGenerator = new Random();
        numGenerator.nextInt();
        char firstLetter = 'A';
        for (int i = 0; i <26; i++){
            alphabet[i] = firstLetter;
            firstLetter++;
        }
        int randomOutput = numGenerator.nextInt(26);
        int randomAlternate = (int)(Math.random() * 26);
        System.out.println(randomAlternate);
        System.out.println(alphabet[randomAlternate]);
        System.out.println("" + (char)(int)(Math.random() * 26 + 65)); ////////////ALTERNATE SOLUTION
    }
    
}
