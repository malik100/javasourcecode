// import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;
import java.util.ArrayList;

public class Database {

    private static final String dbURL = "removed for security";
    private static final String username = "removed for security";
    private static final String password = "removed for security";

    private Connection conn;

    public Database() throws SQLException {
        conn = DriverManager.getConnection(dbURL, username, password);
    }
    
    public ArrayList<Person> getAllPeople() throws SQLException{
        ArrayList<Person> list = new ArrayList<>();
       String sql = "SELECT * from people";
            PreparedStatement statement = conn.prepareStatement(sql);
            try(ResultSet result = statement.executeQuery(sql)){
                while(result.next()){
                    int id = result.getInt("id");
                    String name = result.getString("name");
                    int age = result.getInt("age");
                    //System.out.printf("%d: %s is %d y/o\n", id, name, age);
                    list.add(new Person(id, name, age));
                }
            }  
            return list;
    }
    
    public void addPerson(Person person) throws SQLException{
        String sql = ("insert into people values (null, ?, ?)");
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, person.name);
            statement.setInt(2, person.age);
            statement.executeUpdate();  //for insert update delete
            //System.out.println("Record inserted");
    }
    
    public void updatePerson(Person person) throws SQLException{
            String sql = "update people set name=?, age=? where id=?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, person.name);
            statement.setInt(2, person.age);
            statement.setInt(3, person.id);
            statement.executeUpdate();
            System.out.println("Record updated id =" + person.id);
    }
}
