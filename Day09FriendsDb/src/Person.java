
public class Person {

    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }
    
    int id;
    String name;
    int age;
    
    @Override
    public String toString(){
        return String.format("%s: %s is %d y/o", this.id, this.name, this.age);
    }
}
