
package day02arraylistownimpl;

import java.util.ArrayList;
import java.util.Arrays;


public class Day02ArrayListOwnImpl {

   
    public static void main(String[] args) {
        CustomArrayOfInts dataList = new CustomArrayOfInts();
        
        dataList.add(10);
        dataList.add(20);
        dataList.add(30);
        dataList.add(40);
        dataList.add(50);
        dataList.add(60);
        //System.out.println(Arrays.toString(dataList.getSlice(1, 3)));
        //System.out.println(Arrays.toString(dataList.data));
        //System.out.println(dataList.deleteByValue(40));
        System.out.println(dataList.data.length);
        System.out.println(dataList.size());
        System.out.println(dataList);
    }
    
}
