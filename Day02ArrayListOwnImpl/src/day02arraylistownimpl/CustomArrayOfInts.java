package day02arraylistownimpl;

import java.util.Arrays;

public class CustomArrayOfInts {

    public int[] data = new int[1]; // only grows by doubling size, never shrinks
    private int size = 0; // how many items do you really have

    public int size() {
        return size;
    }

    public void add(int value) {
        System.out.println("Size: " + size + " data.length " + data.length);
        growArray();
        data[size] = value;
        size++;                     //increment size;
    }

    public void deleteByIndex(int index) {
        if(index >= size){
            throw new IndexOutOfBoundsException();
        }
        data[index] = 0;
        for (int i = index; i < data.length - 1; i++) {
            int temp = data[i];
            data[i] = data[i + 1];
            data[i + 1] =  temp;
        }
        size--;
    }

    public boolean deleteByValue(int value) {
        int foundIndex = -1;
        for (int i = 0; i < size; i++) {
            if(data[i] == value){
                foundIndex = i;
                break;
            }
        }
        if(foundIndex == -1) {return false;}
        //data[foundIndex] = 0;
        for (int i = foundIndex; i < data.length - 1; i++) {
            int temp = data[i];
            data[i] = data[i+1];
            data[i+1] = data[i];
        }
        size--;
        return true;
    } 

    public void insertValueAtIndex(int value, int index) {
        if(index > size){
            throw new IndexOutOfBoundsException();
        }
        growArray();
        for (int i = data.length - 1; i > index; i--) {
            int temp = data[i];
            data[i] = data[i-1];
            data[i-1] = temp;
        }
        data[index] = value;
        size++;
    }
/*
    public void clear() {
        size = 0;
    }
*/
    public int get(int index) {
        if(index >= size){
            throw new IndexOutOfBoundsException();
        }
        return data[index];
    } 

    public int[] getSlice(int startIdx, int length) {
        if((startIdx + length) > size){
            throw new IndexOutOfBoundsException();
        }
        int[] slice = new int[length];
        int counter = 0;
        for (int i = startIdx; i < (startIdx + length); i++) {
            slice[counter] = data[i];
            counter++;
        }
        
        return slice;
    }
    
    @Override
    public String toString() {
        // returns String similar to: [3, 5, 6, -23]
        String returnPrint = "[";
        boolean first = false;
        for (int i = 0; i < size; i++) {
           returnPrint += !first ? data[i] : ", " + data[i] ;
            first = true; 
        }
        returnPrint += "]";
        return returnPrint;
    } 

    public void growArray(){
        if(data.length <= size){
            int[] replacement = new int[data.length * 2]; //create new array
            for (int i = 0; i < data.length; i++) {       //populate new array
                replacement[i] = data[i];
            }                         
            this.data = replacement;         //replace reference
        }
    }
}
