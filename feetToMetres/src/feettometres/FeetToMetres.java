/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package feettometres;

import java.util.Scanner;

/**
 *
 * @author john
 */
public class FeetToMetres {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter a value in feet: ");
        double feet = input.nextDouble();
        if (feet < 0){
            System.out.println("wrong input can't enter negative values");
            System.exit(1);
        }
        double metres = feet * 0.305;
        System.out.println(feet + " feet is " + metres + " in Metres");
    }
    
}
