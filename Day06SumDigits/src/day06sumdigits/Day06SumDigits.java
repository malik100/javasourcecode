
package day06sumdigits;


public class Day06SumDigits {

    
    public static void main(String[] args) {
        int input = 123;
        System.out.println(sumDigits(input));
    }
    
    public static int sumDigits(int input){
        int answer = 0;
            while(input != 0){
                int remainder = input % 10;
                answer += remainder;
                input = input / 10;
            }
        return answer;
    }
}
