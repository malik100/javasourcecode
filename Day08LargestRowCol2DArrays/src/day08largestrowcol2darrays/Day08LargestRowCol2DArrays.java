
package day08largestrowcol2darrays;

import java.util.Arrays;


public class Day08LargestRowCol2DArrays {

    
    public static void main(String[] args) {
        int[][]arr = {
            {1,1,1,0,},
            {1,0,1,0,},
            {1,0,0,1,},
            {1,0,0,1,},
        };
//        for (int row = 0; row < arr.length; row++) {
//            for (int col = 0; col < arr[row].length; col++) {
//                arr[row][col] = (int)(Math.random() * 2);
//            }
//        }
        int largestCol = 0;
        int largestRow = 0;
        int col = 0;
        int[]countC = new int[4];
        int[]countR = new int[4];
//        System.out.println(Arrays.deepToString(arr));
////////////////////FOR COLUMN AND ROW//////////////////
        
        for (int row = 0; row < arr.length; row++) {
            for (col = 0; col < arr[row].length; col++) {
                if(arr[row][col] == 1){///////COLUMN
                    countC[col] += 1;
                    countR[row] += 1;
                }
            }
        }
        int biggestTempRow = countR[0];
        for (int i = 0; i < countR.length; i++) {
            if(countR[i] > biggestTempRow){
                biggestTempRow = countR[i];
                largestRow = i;
            }
        }
        ////////////////FOR COLUMN///////////////////
        int biggestTempCol = countC[0];
        for (int i = 0; i < countC.length; i++) {
            if(countC[i] > biggestTempCol){
                biggestTempCol = countC[i];
                largestCol = i;
            }
        }
        System.out.println(Arrays.toString(countR));
                System.out.println(largestRow);
        System.out.println(largestCol);
    }
    
}
