
package day06elimduplicates;

import java.util.Arrays;


public class Day06ElimDuplicates {

    
    public static void main(String[] args) {
        int[] intArray = {1,2,3,2,1,6,3,4,5,2};
        int[] resultArray = new int [intArray.length];
        int uniqueCount = 0;
        int contains = 0;
        for (int i = 0; i < intArray.length; i++) {
            contains = 0;
            for (int j = 0; j < resultArray.length; j++) {
                if(intArray[i] == resultArray[j]){
                    contains = 1;
                }
            }
            if (contains <= 0){
                resultArray[uniqueCount] = intArray[i];
                uniqueCount++;
            }
            
        }
        int[] finalArray = new int[uniqueCount];
        for (int i = 0; i < uniqueCount; i++) {
            finalArray[i] = resultArray[i];
        }
        for( int number:finalArray){
            System.out.println(number);
        }
    }
    
}
