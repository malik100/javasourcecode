
package day01stackinterpreter;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.Stack;


public class Day01StackInterpreter {

    static Stack<Double> stack = new Stack<>();
    public static void main(String[] args) {
        File addtwo = new File("addtwo.txt");
        File cel2fah = new File("cel2fah.txt");
        File fah2cel = new File("fah2cel.txt");
        System.out.println("The following txt files have been found:");
        if(addtwo.exists()){
            System.out.println("addtwo.txt");
        }
        if(cel2fah.exists()){
            System.out.println("cel2fah.txt");
        }
        if(fah2cel.exists()){
            System.out.println("fah2cel.txt");
        }
        
        System.out.println("Enter the name of the file you want to use");
        Scanner input = new Scanner (System.in);
        String fileName = input.nextLine();
        
        switch(fileName){
            case "addtwo.txt" :
                System.out.println("you chose addtwo");
                break;
            case "cel2fah.txt" :
                System.out.println("You chose cel2fah");
                break;
            case "fah2cel.txt" :
                System.out.println("You chose fah2cel");
                break;
            default :
                System.out.println("Incorrect input please try again");
        }
        
        //read push pop print push() pop() peek() size()
        
        try(Scanner read = new Scanner(new File (fileName))){
            while(read.hasNextLine()){
                String line = read.nextLine();
                String[] commands = line.split(":");
                String task = commands[0];
                System.out.println(task);
                if(task.equals("read")){
                    System.out.println(commands[1]);
                    stack.add(input.nextDouble());
                    System.out.println("<<Current stack: " + stack + " >>");
                }else if(task.equals("push")){
                    stack.push(Double.parseDouble(commands[1]));
                }else if(task.equals("pop")){
                    if(stack.size() >= 1){
                        stack.pop();
                    }else{
                        System.out.println("stack has no values currently");
                    }
                }else if(task.equals("print")){
                    System.out.println(commands[1]);
                    if(stack.size() >=1 ){
                        System.out.println(stack.peek());
                    }else{
                        System.out.println("stack has no values to peek");
                    }
                }else if(task.equals("add")){
                    stack.push(stack.pop() + stack.pop());
                }else if(task.equals("sub")){
                }else if(task.equals("mul")){
                }else if(task.equals("div")){
                }
                
            }
            
        }catch(IOException i){
            System.out.println("File was not found"); 
        }
        
    }
    
}
