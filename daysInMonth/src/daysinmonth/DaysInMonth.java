/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daysinmonth;

import java.util.Scanner;

/**
 *
 * @author john
 */
public class DaysInMonth {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int [] monthDays = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        String [] monthNames = {"january", "february", "march", "april", "may", "june", "july",
                "august", "september", "october", "november", "december"};
        Scanner input = new Scanner(System.in);
        System.out.println("Enter a month number to see how many days it has: ");
        int monthInput = input.nextInt() - 1;
        if (monthInput < 0){
            System.out.println("wrong input can't enter negative values");
            System.exit(1);
        }
        System.out.println(monthNames[monthInput] + " has " + monthDays[monthInput] + " days ");
        }
    
        
    
}
