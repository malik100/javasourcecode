package finaltreepatterns;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

class DuplicateValueException extends Exception {
}

public class TreeStringIntSet implements Iterable<Pair<String, Integer>> {

    
    class IterateHolder implements Iterator<Pair<String, Integer>> {  //class to implement iterator methods

        private Pair[] pairArr;    //private fields for this class
        private int index;

        public IterateHolder(Pair[] pairArr) {   //constructor
            this.pairArr = pairArr;
        }

        
        @Override
        public boolean hasNext() {
            return index < pairArr.length;
        }

        @Override
        public Pair<String, Integer> next() {
            return pairArr[index++];
        }

    }

    @Override
    public Iterator<Pair<String, Integer>> iterator() {
        return new IterateHolder(getAllPairs()); //construct and return class with all pairs from this tree      
    }
    //public Iterator<Pair<String,Integer>> iterator() { } // needed for iterator (DUPLICATE)
    
    //TreeStringIntSet

    private class Node {

        Node left, right;
        String key; // keys are unique
        // HashSet is like ArrayList except it does not hold duplicates
        HashSet<Integer> valuesSet = new HashSet<>(); // unique only

        public Node(String key) {   //constructor for node
            this.left = left;
            this.right = right;
            this.key = key;
        }

    }

    private Node root;
    private int nodesCount;   // private helper property used in other helper methods
    
    // throws DuplicateValueException if this key already contains such value

    public void add(String key, int value) throws DuplicateValueException {
        Node newNode = new Node(key);
        newNode.valuesSet.add(value);   //assgin root if first entry in tree
        if (root == null) {
            root = newNode;
            nodesCount++;
            return;
        }
        Node current = root;
        while (current != null) {             //iterate trhough tree to find place for new/existing node
            if (newNode.key.compareTo(current.key) == 0) {
                if (current.valuesSet.contains(value)) {
                    throw new DuplicateValueException();  //duplicate key and value
                }
                current.valuesSet.add(value);  //add value to existing node if not duplictae
                return;
            }
            if (newNode.key.compareTo(current.key) > 0 && current.right == null) {   
                current.right = newNode; //if new key is bigger and right node is null assign it to right node
                nodesCount++;

                return;
            }
            if (newNode.key.compareTo(current.key) < 0 && current.left == null) {
                current.left = newNode;  //if new key is smaller and left node is null assign it to left node
                nodesCount++;
                return;
            }
            //compare key and move onto left/right node
            current = newNode.key.compareTo(current.key) > 0 ? current.right : current.left;
        }
    }

    public boolean containsKey(String key) {
       if (root == null) {
            return false;  //if no values in tree return false immediately
        }
        Node current = root;
        while (true) {
            if (current == null) {   //end of tree reached value not found
                return false;
            }
            if (current.key.toString().equals(key.toString())) {
                return true;   //match found
            }
            //iterate left or right based on if key is bigger or smaller
            current = key.toString().compareTo(current.key.toString()) > 0 ? current.right : current.left;
        }
    }
    // return empty list if key not found
    // returns keys sorted
    public List<Integer> getValuesOfKey(String key) {
        valuesCollected.clear();  //empty old values
        if (root == null) {
            return valuesCollected;  //empty tree
        }
        Node current = root;
        while (true) {
            if (current == null) {  //end of tree reached 
                valuesCollected.sort((o1, o2) -> (o1 > o2) ? 1 : (o1 < o2) ? -1 : 0); //sort values
                return valuesCollected;   //return values even if empty
            }
            if (current.key.toString().equals(key.toString())) {
                for (int i : current.valuesSet) {
                    valuesCollected.add(i);           //if match found iterate over valueset and add it
                }
                valuesCollected.sort((o1, o2) -> (o1 > o2) ? 1 : (o1 < o2) ? -1 : 0); //sort values
                return valuesCollected;   //return without iterating over rest of tree
            }
            //go to next node based on if key bigger or smaller
            current = key.toString().compareTo(current.key.toString()) > 0 ? current.right : current.left;
        }
    }

    // use traversal of all nodes to collect keys that have this value in valuesSet
    public List<String> getKeysContainingValue(int value) {
        keysCollected.clear();
        getKeyContainValueHelper(root, value);  //call private helper method
        return keysCollected;
    }

    private void getKeyContainValueHelper(Node n, int value) {
        if (n == null) {
            return;
        }
        getKeyContainValueHelper(n.left, value);  //recursive method on all nodes smaller
        if (n.valuesSet.contains(value)) {
            keysCollected.add(n.key); //adding key to list
        }
        getKeyContainValueHelper(n.right, value); //recursion on all nodes bigger

    }

    //private fields for helper methods
    private List<String> keysCollected = new ArrayList<>();
    private List<Integer> valuesCollected = new ArrayList<>();
    Pair[] pairs;
    int pairIndex;
    
    private Pair[] getAllPairs(){    //helper method used to send pairs to iterator (used in constructor)
        pairs = new Pair[nodesCount];
        pairHelper(root);
        return pairs;
    }

    private void pairHelper(Node n){  //works with above method to collect pairs
        if(n == null)return;
        pairHelper(n.left);
        pairs[pairIndex] = new Pair(n.key, n.valuesSet);  //add pair to array
        pairIndex++;
        pairHelper(n.right);
    }
    
    // use traversal to collect all keys
    public List<String> getAllKeys() {
        keysCollected.clear();
        getKeyHelper(root);   //calling private helper method to collect keys
        return keysCollected;
    }

    private void getKeyHelper(Node n) {   //helper method recursively collects keys smallext to biggest
        if (n == null) {
            return;
        }
        getKeyHelper(n.left);  //recursive method on all nodes smaller
        keysCollected.add(n.key); //adding key to list
        getKeyHelper(n.right); //recursion on all nodes bigger

    }
     
    
    
    //You can add toString() for debugging if you like
    // You can add other private methods and fields as needed. But no public ones.
    // Important: you are NOT allowed to modify the signatures of any public method given above
/*
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        return "";
    }
*/
}

class Pair<K, V> {

    K key;
    V value;

    Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String toString() {
        return String.format("(%s=>%s)", key.toString(), value.toString());
    }
}
