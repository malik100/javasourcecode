/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finaltreepatterns;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class TreeStringIntSetTest {
    
    

    
    @Test
    public void testIterator() throws DuplicateValueException {
        System.out.println("iterator");
        TreeStringIntSet tree = new TreeStringIntSet();
        tree.add("Eva", 10);
        tree.add("Tom", 100);
        tree.add("Abe", 5);
        tree.add("Eva", 12);
        tree.add("Eva", 8);
        tree.add("Eva", 23);
        tree.add("Ken", 12);
        StringBuilder sb = new StringBuilder();    //stringbuilder will append the toString of all items in foreach
        for (Pair<String, Integer> p : tree){    //calling the iterator to append each item
            sb.append(":" + p);
        }
        assertEquals(":(Abe=>[5]):(Eva=>[23, 8, 10, 12]):(Ken=>[12]):(Tom=>[100])", sb.toString());
        
        //also partially tests if values are inserted in correct place because it traverses left to right and values
        //will be in alphabetical order if inserted properly
    }

    
    
    @Test
    public void testAdd() throws Exception {
        System.out.println("add");
        TreeStringIntSet tree = new TreeStringIntSet();
        tree.add("Eva", 10);
        assertEquals("[Eva]", tree.getAllKeys().toString()); //1 item expected
        tree.add("Tom", 100);
        tree.add("Tom", 50);
        tree.add("Tom", 10);    //Also tests that duplicates values on different key don't throw exception (Tom->10, Eva->10)
        assertEquals("[Eva, Tom]", tree.getAllKeys().toString());   //2 items
        tree.add("Abe", 5);
        assertEquals("[Abe, Eva, Tom]", tree.getAllKeys().toString());  //3 items
        
    }
    
    @Test (expected = DuplicateValueException.class)
    public void testAddException() throws Exception {
        System.out.println("add");
        TreeStringIntSet tree = new TreeStringIntSet();
        tree.add("Eva", 10);  //10 added to key Eva
        tree.add("Tom", 100);
        tree.add("Tom", 50);
        tree.add("Abe", 5);
        tree.add("Eva", 10);   //expected exception here
    }
    
    @Test
    public void containsKey() throws Exception {
        System.out.println("add");
        TreeStringIntSet tree = new TreeStringIntSet();
        tree.add("Eva", 10);
        assertEquals(false, tree.containsKey("Tom"));   //false
        tree.add("Tom", 100);
        assertEquals(true, tree.containsKey("Tom"));   //true
        tree.add("Tom", 50);
        tree.add("Abe", 5);
        assertEquals(true, tree.containsKey("Eva"));   //true
        assertEquals(false, tree.containsKey("aaaaaa"));   //false
    }
    
    @Test
    public void testGetValuesOfKey() throws DuplicateValueException {
        System.out.println("getValuesOfKey");
        TreeStringIntSet tree = new TreeStringIntSet();
        tree.add("Eva", 10);
        assertEquals("[10]" , tree.getValuesOfKey("Eva").toString());  //should have 1
        tree.add("Tom", 100);
        tree.add("Tom", 50);
        assertEquals("[50, 100]" , tree.getValuesOfKey("Tom").toString());  //should have 2 sorted
        tree.add("Abe", 5);
        tree.add("Eva", 15);  //larger value added before and after samller to test sorting: Eva->(8, 15, 12) unsorted order
        tree.add("Eva", 12);
        assertEquals("[10, 12, 15]" , tree.getValuesOfKey("Eva").toString());  //should have 3 items sorted samll to large
        assertEquals("[]" , tree.getValuesOfKey("abc").toString());  //should have empty array
        
    }

    @Test
    public void testGetKeysContainingValue() throws DuplicateValueException {
        System.out.println("getKeysContainingValue");
        int value = 0;
        TreeStringIntSet tree = new TreeStringIntSet();
        tree.add("Eva", 10);
        tree.add("Tom", 100);
        tree.add("Abe", 5);
        tree.add("Eva", 12);  //searched value (12) key Eva
        tree.add("Eva", 8);
        tree.add("Eva", 23);
        tree.add("Ken", 12); //searched value (12)  key Ken
        ArrayList<String> expRes = new ArrayList();  //new List with the expected values 
        expRes.add("Eva");
        expRes.add("Ken");
        assertEquals(expRes, tree.getKeysContainingValue(12));
    }
    

    @Test
    public void testGetAllKeys() throws DuplicateValueException {
        System.out.println("getAllKeys");
        TreeStringIntSet tree = new TreeStringIntSet();
        tree.add("Eva", 10);  //Eva
        tree.add("Tom", 100); //Tom
        tree.add("Abe", 5);   //Abe
        tree.add("Eva", 12);
        tree.add("Eva", 8);
        tree.add("Eva", 23);
        tree.add("Ken", 12);  //Ken
        tree.add("Lenny", 55); //Lenny
        ArrayList<String> expRes = new ArrayList();  //new List with the expected values 
        expRes.add("Abe");
        expRes.add("Eva");
        expRes.add("Ken");
        expRes.add("Lenny");
        expRes.add("Tom");
        assertEquals(expRes, tree.getAllKeys());
    }
    
    
    
}
