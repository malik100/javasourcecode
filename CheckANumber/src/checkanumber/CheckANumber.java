/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package checkanumber;

import java.util.Scanner;

/**
 *
 * @author john
 */
public class CheckANumber {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter a whole number greater than zero: ");
        int numberEntered = input.nextInt();
        boolean divisibleByFive = false;
        boolean divisibleBySix = false;
        if (numberEntered <= 0){
            System.out.println("wrong input enter a value above zero");
            System.exit(1);
        }
        if (numberEntered % 5 == 0){
            divisibleByFive = true;
        }
        if (numberEntered % 6 == 0){
            divisibleBySix = true;
        }
        if (divisibleByFive == true && divisibleBySix == true){
            System.out.println(numberEntered + " is divisible by both 5 and 6");
        }
        else if(divisibleByFive == true || divisibleBySix == true ){
            System.out.println(numberEntered + " is divisible by 5 or 6 but not both");
        }
        else{
            System.out.println(numberEntered + " is not divisible by either 5 or 6");
        }
    }
    
}
