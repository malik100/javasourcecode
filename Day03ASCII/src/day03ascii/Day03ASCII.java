/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03ascii;

/**
 *
 * @author john
 */
public class Day03ASCII {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {  //int to char
        int valueEntered = 121;
        char c = (char)valueEntered;  
        System.out.printf("The character is %c\n", c);
        System.out.println((char)valueEntered); //>>ALTENATE SOLUTION<<
    }
    
}
