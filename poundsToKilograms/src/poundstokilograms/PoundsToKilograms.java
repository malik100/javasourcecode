/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poundstokilograms;
import java.util.Scanner;
/**
 *
 * @author john
 */
public class PoundsToKilograms {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Enter a value in pounds: ");
        Scanner input = new Scanner(System.in);
        double pounds = input.nextDouble();
        if (pounds < 0){
            System.out.println("wrong input can't enter negative values");
            System.exit(1);
        }
        double kilograms = pounds * 0.454;
        System.out.println(pounds + " pounds is " + kilograms + " kilograms." );
    }
    
}
