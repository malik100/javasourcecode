
package day07overloadedmethodaverage;


public class Day07OverloadedMethodAverage {

    
    public static void main(String[] args) {
        double[] arrayD = {10.0,20.0,30.0,40.0,50.0};
        int[] arrayI = {60,70,80,90,100};
        System.out.println(average(arrayI));
    }
    
    public static int average(int[] array){
        int result = 0;
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        result = (int)((double)sum / array.length);
        return result;
    }
    public static double average(double[] array){
        double result = 0;
        double sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        result = sum / array.length;
        return result;
    }
}
