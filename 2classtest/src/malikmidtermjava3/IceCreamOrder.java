package malikmidtermjava3;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import static malikmidtermjava3.MalikMidtermJava3.fullUseableDate;
import static malikmidtermjava3.MalikMidtermJava3.input;

public class IceCreamOrder {

    //Constructors------------------------------------------------------------
    public IceCreamOrder(String dataLine) throws DataInvalidException {
        String data[] = dataLine.split(";");
        if (data.length != 3) {
            throw new DataInvalidException("Invalid number of items in line");
        }
        //validating and assigning date-----------------------------------------
        String[] dateData = data[1].split(" at ");
        if (data.length != 3) {
            throw new DataInvalidException("Invalid number of items in line");
        }
        String datePortion = dateData[0];                                  // Datainvalid exception possible
        String timePortion = dateData[1];                                  // Datainvalid exception possible
        String completeDate = datePortion + " " + timePortion;
        Date date;
        try {
            date = fullUseableDate.parse(completeDate);                   //ParseException possible (throws to main)
        } catch (ParseException ex) {
            throw new DataInvalidException("Unable to parse date");
        }
//        for (int i = 0; i < data.length; i++) {
//            System.out.println(data[i]);
//        }
        //validating and assigning flavour-----------------------------------------
        String[] flavours = data[2].split(",");
        ArrayList<Flavour> flv = new ArrayList<>();
        for (int i = 0; i < flavours.length; i++) {
            String f = flavours[i].toUpperCase();
            flv.add(Flavour.valueOf(f));
        }
        setCustomerName(data[0]);
        setDeliveryDate(date);
        setFlavList(flv);
    }

    public IceCreamOrder(String custName, Date delivDate, ArrayList<Flavour> flavList) throws DataInvalidException {
        setCustomerName(custName);                                           //DataInvalidException possible for all 3
        setDeliveryDate(delivDate);
        setFlavList(flavList);
    }

    //Setters---------------------------------------------------------------
    public void setCustomerName(String customerName) throws DataInvalidException {   //handled in main
        if (!customerName.matches("^[a-zA-Z0-9() ,.'\'-]{2,20}")) {                    //DataInvalidException possible
            throw new DataInvalidException("Name must be 2-20 characters long and only contain alphaneumeric characters or ,.'\"()- or space");
        }
        this.customerName = customerName;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public void setFlavList(ArrayList<Flavour> flavList) throws DataInvalidException {   //handled in main
        if (flavList.isEmpty()) {                                                         //DataInvalidException possible
            throw new DataInvalidException("Floavor list must not be empty");
        }
        this.flavList = flavList;
    }

    //Getters-------------------------------------------------------------------
    public ArrayList<Flavour> getFlavList() {
        return flavList;
    }

    public String getCustomerName() {
        return customerName;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    @Override
    public String toString() {
        String datePortion = MalikMidtermJava3.useableDate.format(deliveryDate);
        String timePortion = MalikMidtermJava3.useableTime.format(deliveryDate);
        return "for " + customerName + ", delivery on " + datePortion + " at " + timePortion + " flavours: " + flavList;
    }
//Enum-----------------------------------------------------------------------

    public String toDataString() {
        String datePortion = MalikMidtermJava3.useableDate.format(deliveryDate);
        String timePortion = MalikMidtermJava3.useableTime.format(deliveryDate);
        String flv = "";
        for (int i = 0; i < flavList.size(); i++) {
            flv += flavList.get(i);
            if(i != flavList.size() - 1){
                flv += ",";
            }
        }
        return this.customerName + ";" + datePortion +
                " at " + timePortion + ";" + flv;
    }

    enum Flavour {
        VANILLA, CHOCOLATE, STRAWBERRY, ROCKYROAD
    }
    //Fields-----------------------------------------------------------------
    private String customerName;
    private Date deliveryDate;
    private ArrayList<Flavour> flavList = new ArrayList();

}
