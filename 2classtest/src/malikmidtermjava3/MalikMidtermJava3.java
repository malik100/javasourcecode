package malikmidtermjava3;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import malikmidtermjava3.IceCreamOrder.Flavour;

public class MalikMidtermJava3 {

    //Static items
    static final String FILE_PATH = "orders.txt";
    static ArrayList<IceCreamOrder> ordersList = new ArrayList<>();
    static Scanner input = new Scanner(System.in);
    static SimpleDateFormat useableDate = new SimpleDateFormat("yyyy-MM-dd");
    static SimpleDateFormat useableTime = new SimpleDateFormat("hh:mm");
    static SimpleDateFormat fullUseableDate = new SimpleDateFormat("yyyy-MM-dd hh:mm");

    static {
        useableDate.setLenient(false);
    }

    static {
        useableTime.setLenient(false);
    }

    static {
        fullUseableDate.setLenient(false);
    }

    public static void main(String[] args) {
        try {
            loadDataFromFile("input.txt");
        } catch (DataInvalidException ex) {
            System.out.println("File IO error " + ex.getMessage());
        }
        while (true) {
            try {
                int choice = getMenuChoice();
                switch (choice) {
                    case 1:
                        addOrder();
                        break;
                    case 2:
                        System.out.println("Current orders by customer name");
                        ordersByName(ordersList);
                        break;
                    case 3:
                        System.out.println("Current orders by order date");
                        ordersByDate();
                        break;
                    case 0:
                        saveDataToFile();
                        System.out.println("Exiting.");
                        return;
                    default:
                        System.out.println("Invalid choice, try again");
                }
                System.out.println();
            } catch (DataInvalidException ex) {                            //Catches exceptions from higher level of loop
                System.out.println("Error: " + ex.getMessage());
                input.nextLine(); // catch \n token etc.
            }

        }
    }

    static int inputInt() throws DataInvalidException {
        while (true) {
            try {
                int result = input.nextInt();                           // InputMismatchException possible
                input.nextLine();
                return result;
            } catch (InputMismatchException ex) {
                throw new DataInvalidException("Please enter only a non decimal number"); //Chain exception Inputmismatch  >> Datainvalid
            }
        }
    }

    static int getMenuChoice() throws DataInvalidException {
        System.out.print("Please make a choice [0-]:\n"
                + "1. Add an order\n"
                + "2. List order by customer name\n"
                + "3. List order by delivery date\n"
                + "0. Exit\n"
                + "Your choice is: ");
        int choice = inputInt();
        return choice;
    }

    private static void addOrder() throws DataInvalidException {
        try {
            System.out.println("You have chosen to add a new order");
            System.out.println("Please Enter your name (2-20 alphaneumeric chracters and ,.\"'()- and space");
            String name = input.nextLine();                                         //DataInvalidException possible
            System.out.println("Please enter the delivery (yyyy-mm-dd)");
            String datePortion = input.nextLine();                                  // Datainvalid exception possible
            System.out.println("Please enter delivery time (hh:mm)");
            String timePortion = input.nextLine();                                  // Datainvalid exception possible
            String completeDate = datePortion + " " + timePortion;
            Date date = fullUseableDate.parse(completeDate);                   //ParseException possible
            System.out.println("Please pick a flavour from the following enter an empty line to finish");
            for (Flavour fla : Flavour.values()) {
                System.out.println(fla);
            }
            String flavour = "";
            ArrayList<Flavour> flavourChoice = new ArrayList();
            while(true){
                flavour = input.nextLine().toUpperCase();    //changes to upper case to match enum   //IllegalArgumentException possible
                if(flavour.equals("")){
                    break;
                }
                verifyFlavour(flavour);                                                 // Datainvalid exception possible
                flavourChoice.add(Flavour.valueOf(flavour));
            }
            IceCreamOrder newOrder = new IceCreamOrder(name, date, flavourChoice);
            ordersList.add(newOrder);
            //System.out.println(newOrder);
            //System.out.println(ordersList);
        } catch (ParseException p) {
            throw new DataInvalidException("Error parsing the date");           //chaining Parseexception >> DataInvalivException
        } catch (IllegalArgumentException il) {
            throw new DataInvalidException("That value is not in the list of flvours"); //chaining IllegalArgument >> DataInvalid exception
        }

    }

    public static boolean verifyFlavour(String flavChoice) throws DataInvalidException {  //used to verify correct enum type
        int count = 0;
        // iterating through flavours to get size
        for (Flavour fla : Flavour.values()) {
            count++;
        }
        //checking value to see if it matches with enum
        for (int i = 0; i < count; i++) {
            for (Flavour fla : Flavour.values()) {
                if (flavChoice.equalsIgnoreCase(flavChoice)) {
                    return true;
                }
            }
        }
        throw new DataInvalidException("The flavour you entered did not match our list returning to main menu");
    }

    private static void ordersByName(ArrayList<IceCreamOrder> list) {
        //Sorting by comparing name with lambda formula
        ordersList.sort((IceCreamOrder o1, IceCreamOrder o2) -> o1.getCustomerName().compareTo(o2.getCustomerName()));
        for (int i = 0; i < ordersList.size(); i++) {
            System.out.println("#" + (i + 1) + " " + ordersList.get(i));
        }
    }

    private static void ordersByDate() {
        ordersList.sort((IceCreamOrder o1, IceCreamOrder o2) -> o1.getDeliveryDate().compareTo(o2.getDeliveryDate()));
        for (int i = 0; i < ordersList.size(); i++) {
            System.out.println("#" + (i + 1) + " " + ordersList.get(i));
        }
    }

    static void loadDataFromFile(String path) throws DataInvalidException{
        try (Scanner reader = new Scanner(new File(path))) {
            while (reader.hasNextLine()) {
                try {
                    String dataLine = reader.nextLine();
                    ordersList.add(new IceCreamOrder(dataLine));
                } catch (NumberFormatException n) {
                    throw new DataInvalidException("a neumerical error occured while trying to read file");
                } catch (IllegalArgumentException il) {
                    throw new DataInvalidException("incorrect data type when reading external file");
                } catch (DataInvalidException ex) {
                    System.out.println(("An IO error Occured"));        //handles loop level exceptions-keeps loop continuing
                }
            }

        } catch (IOException io) {
            throw new DataInvalidException("An IO error Occured");      //throws file IO level exception to main where it is handeled
        }

    }

    private static void saveDataToFile() {
        try(PrintWriter out = new PrintWriter(new File ("output.txt"))){
            for(IceCreamOrder ice : ordersList){
                out.println(ice.toDataString());
            }
        }catch(IOException i){
            System.out.println("IO error during process: " + i.getMessage());
        }
    }
}
