
package day08sumcolumn2darray;


public class Day08SumColumn2DArray {

    
    public static void main(String[] args) {
        double[][] arr = {
                {1.5, 2, 3, 4},
                {5.5, 6, 7, 8},
                {9.5, 1, 3, 1},
        };
        int colIndex = 0;
        double sum = 0;
        for (int row = 0 ; row < arr.length; row++) {
            for (int col = 0; col < arr[row].length; col++) {
                if(col == colIndex){
                    sum += arr[row][col];
                }
            }
        }
        System.out.println(sum);
        
        
        
    }
    
}
