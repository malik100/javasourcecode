
package dsday08binarytreekeyval;


public class DSDay08BinaryTreeKeyVal {

    
    public static void main(String[] args) {
        BinaryTreeStringInt bt = new BinaryTreeStringInt();
        bt.put("g", 90);
        bt.put("a", 33);
        bt.put("c", 66);
        bt.put("i", 12);
        bt.put("h", 5);
        bt.put("b", 4);
        bt.put("d", 100);
        System.out.println("NodesCount: " + bt.nodesCount);
        bt.printAllKeyValPairs();
        String arr[] = bt.getKeysInOrder();
        for(String s : arr){
            System.out.println(s + " :");
        }
        BinaryTreeStringInt.Pair[] p = bt.getKeyValuePairsInOrder();
        for(BinaryTreeStringInt.Pair pair : p){
            System.out.printf("%s--%s\n", pair.key, pair.value);
        }
    }
    
}
