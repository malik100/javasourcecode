package dsday08binarytreekeyval;

public class BinaryTreeStringInt {

    public class Node {

        String key;
        int value;
        Node left, right;

        public Node(String key, int value, Node left, Node right) {
            this.key = key;
            this.value = value;
            this.left = left;
            this.right = right;
        }

    }

    class Pair {

        String key;
        int value;

        public Pair(String key, int value) {
            this.key = key;
            this.value = value;
        }

    }

    public Node root;
    public int nodesCount;

    // if put attempts to put a key that already exists then value is updated (no exception is thrown)
    // values may be duplicates but keys may not
    void put(String key, int value) {
        Node newNode = new Node(key, value, null, null);
        if (nodesCount == 0) {
            root = newNode;
            nodesCount++;
            return;
        }
        Node current = root;
        while (current != null) {
            if (newNode.key.compareTo(current.key) == 0) {  //same item new value added
                current.value = value;
                return;
            }
            if (newNode.key.compareTo(current.key) > 0 && current.right == null) { //item bigger and place found
                current.right = newNode;
                nodesCount++;

                return;
            }
            if (newNode.key.compareTo(current.key) < 0 && current.left == null) { //item smaller and place found
                current.left = newNode;
                nodesCount++;
                return;
            }
            //go left or right in tree
            current = newNode.key.compareTo(current.key) > 0 ? current.right : current.left;
        }

    }

    // print out all key-value pairs (one per line) from the smallest key to the largest
    void printAllKeyValPairs() {
        keyHelper(root, 0);
    }
/*
    private void printHelper(Node n) {
        if (n == null) {
            return;
        }
        if (n.left != null) {
            printHelper(n.left);
        }
        System.out.println(">>> " + n.key);
        if (n.right != null) {
            printHelper(n.right);
        }
    }
*/
    private int index;
    private String[] arr;
    private Pair[] pairArr;

    private void keyHelper(Node n, int caller) {
        if (n == null) return;
        if (n.left != null) {
            keyHelper(n.left, caller);
        }
        //----------------------------------------------------------------------
        if (caller == 1) {
            arr[index] = n.key;
            index++;
        }else if(caller == 2){
            pairArr[index] = new Pair(n.key, n.value);
            index++;
        }else{
            System.out.println(n.key + " <><><>");
        }
        //----------------------------------------------------------------------
        if (n.right != null) {
            keyHelper(n.right, caller);
        }

    }

    // return all keys from smallest to largest (alphabetically)
    String[] getKeysInOrder() {
        index = 0;
        arr = new String[nodesCount];
        keyHelper(root, 1);
        return arr;
    }

    // return pairs of key-value in order of keys
    Pair[] getKeyValuePairsInOrder() {
        index = 0;
        pairArr = new Pair[nodesCount];
        keyHelper(root, 2);
        return pairArr;
    }

}
