/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day04maxoccurence;

/**
 *
 * @author john
 */
public class Day04MaxOccurence {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int max = Integer.MIN_VALUE;
        int count = 0;
        int[] numInput = {3, 5, 2, 5, 5, 5 ,0};
        
        for (int i = 0; i < numInput.length; i++) {
            if (numInput[i] > max){
                max = numInput[i];
                count = 0;
            }
            if(numInput[i] == max){
                count++;
            }
        }
        System.out.printf("value is %d and occurs %d times\n", max, count);
    }
    
}
