
package dsday02jaggedarrays;

import java.util.Arrays;


public class DSDay02JaggedArrays {

    
    public static void main(String[] args) {
        int[][] arr = new int[][]{
            {1, 2, 3, 4},
            {5, 6},
            {7, 8, 9}
        };
        //int[] sum = colSum(arr);
        //System.out.println(sumOfCross(arr, 0, 0));
        int[][] newArr = new int[arr.length][];
        replicateArray(arr, newArr);
        sumPopulateArray(newArr, arr);
        System.out.println(Arrays.deepToString(newArr));
    }
    
    public static void sumPopulateArray(int[][] arr1, int[][] arr2){
        for (int row = 0; row < arr1.length; row++) {
            for (int col = 0; col < arr1[row].length; col++) {
                arr1[row][col] = sumOfCross(arr2, row, col);
            }
        }
    }
    
    public static int sumOfCross(int[][] arr, int row, int col){
        int sum = arr[row][col];
        int leftY = col -1;
        int leftX = row;
        if(ifExists(arr, leftX, leftY) > 0){ sum += arr[leftX][leftY];}
        int rightY = col +1;
        int rightX = row;
        if(ifExists(arr, rightX, rightY) > 0){ sum += arr[rightX][rightY];}
        int aboveY = row - 1;
        int aboveX = col;
        if(ifExists(arr, aboveY, aboveX) > 0){sum += arr[aboveY][aboveX];}
        int belowY = row + 1;
        int belowX = col;
        if(ifExists(arr, belowY, belowX) > 0){sum += arr[belowY][belowX];}
        return sum;
    }
    
    public static int ifExists(int[][] arr, int row, int col ){
        if(arr.length < row + 1 || row < 0){
            return 0;
        }
        if(arr[row].length < col + 1|| col < 0){
            return 0;
        }
        return 1;
    }
    
    
    public static int[] colSum(int[][] arr){
        int[] sum = new int[arr[0].length];
        int colSum = 0;
        for (int col = 0; col < arr[0].length; col++) {
            colSum = 0;
            for (int row = 0; row < arr.length; row++) {
                if(!(col >= arr[row].length)){
                   colSum += arr[row][col]; 
                }
            }
            sum[col] = colSum;
            System.out.println(sum[col]);
        }
        return sum;
    }
    
    public static int sumArray(int[][] arr){
        int sum = 0;
        for (int row = 0; row < arr.length; row++) {
            for (int col = 0; col < arr[row].length; col++) {
                sum += arr[row][col];
            }
        }
        return sum;
    }
    
    public static int[] rowSum(int[][] arr){
        int[] sum = new int[maxLength(arr)];
        for (int row = 0; row < arr.length; row++) {
            for (int col = 0; col < arr[row].length; col++) {
                sum[row] += arr[row][col];
            }
            System.out.println("Row " + (row + 1) + " is " + sum[row]);
        }
        return sum;
    }
    
    public static int maxLength(int[][] arr){
        int max = -1;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if(arr[i].length > max){
                    max = arr[i].length;
                }
            }
        }
        return max;
    }
    
    public static void replicateArray(int[][] arrOne, int[][] arrTwo){
        for (int row = 0; row < arrOne.length; row++) {
            for (int col = 0; col < arrOne[row].length; col++) {
                arrTwo[row] = new int [arrOne[row].length];
            }
        }
    }
    
}
