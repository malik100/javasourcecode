package dsday03linkedlistarray;


public class LinkedListArrayOfStrings {
    public class Container {
		Container next;
		String value;
                public Container(Container next, String value){
                    this.next = next;
                    this.value = value;
                }
	}
	public Container start;
        public Container end;
	public int size;

	public void add(String value) {
            Container newCon = new Container(null, value); //new Container
            if(start == null){
                start = newCon;
                end = null;
                size++;
                return;
            }
            Container current = start;
            while(current.next != null){
                current = current.next;
            }
            current.next = newCon;
            this.end = current.next;
            size++;
        }
        
	public String get(int index) {
            if(index >= this.size || start == null || index < 0){
                throw new IndexOutOfBoundsException();
            }
            Container current = start;
            for (int i = 0; i < size; i++) {
                if(i == index){
                    return current.value;
                }
                current = current.next;
            }
            return "";
        }
        
	public void insertValueAtIndex(int index, String value) {
            if(index < 0 || index >= size){
                throw new IndexOutOfBoundsException();
            }
            //if insertion is in index 0
            if (index == 0){
                Container cont = new Container(start, value);
                start = cont;
                return;
            }
            if(index == size - 1){
                add(value);
                return;
            }
            Container current = start;
            for (int i = 0; i < index - 1; i++) {
                current = current.next;
            }
            current.next = new Container(current.next, value);
            size++;
        }
	public void deleteByIndex(int index) { }
        /*
	public boolean deleteByValue(String value) {} // delete first value found
	public int getSize() { }
	*/
	@Override
	public String toString() {
            String asString = "[";
            if(start == null){return "[]";}
            Container current = start;
            asString += current.value;
            while(current.next != null){
                current = current.next;
                asString += ", " + current.value;
            }
            asString += "]";
        return asString;
        } 
/*
	public String[] toArray() { } // could be used for Unit testing
*/
}
