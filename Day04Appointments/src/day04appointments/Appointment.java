package day04appointments;


import day04appointments.DataInvalidException;
import static day04appointments.Day04Appointments.useableDate;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
enum Reason {
        CHECKUP, REFERRAL, TESTS, FOLLOWUP, UNWELL
    }
public class Appointment {

    Appointment(Date date, int durMin, String name, HashSet<Reason> reasonList) throws DataInvalidException {
        setDate(date);
        setDurMin(durMin);
        setName(name);
        setReasonList(reasonList);
    }

    

    public void setDate(Date date) throws DataInvalidException {
        Date current = new Date();
        String[] currArr = useableDate.format(current).split("-");
        String[] appArr = useableDate.format(date).split("-");
        if(false){
            throw new DataInvalidException("Appointment day must be tomorrow or after.");
        }
        System.out.println("The date that was set " + date);
        this.date = date;
    }

    public void setDurMin(int durMin) throws DataInvalidException {
        if (durMin == 20 || durMin == 40 || durMin == 60) {
            this.durMin = durMin;
        } else {
            throw new DataInvalidException("Minutes duration must be 20, 40 or 60");
        }

    }

    public void setName(String name) throws DataInvalidException {
        if (!name.matches("^[A-Za-z0-9(),'\". -]+$")) {
            throw new DataInvalidException("Name can only contain alpanumuric charachters and valid symbols");
        }
        this.name = name;
    }

    public void setReasonList(HashSet<Reason> reasonList) throws DataInvalidException {
        if(reasonList.isEmpty()){
            throw new DataInvalidException("The reason list must not be empty when set");
        }
        this.reasonList = reasonList;
    }

    
    
    //GETTERS------------------------------------------------------
    public int getDurMin() {
        return durMin;
    }

    public String getName() {
        return name;
    }

    public HashSet<Reason> getReasonLis() {
        return reasonList;
    }

    public Date getDate() {
        return date;
    }
    // Enum-------------------------------------------------------
    
    //Fields------------------------------------------------------
    private Date date;
    private int durMin;
    private String name;
    private HashSet<Reason> reasonList = new HashSet<>();

}
