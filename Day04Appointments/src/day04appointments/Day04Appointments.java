package day04appointments;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Day04Appointments {

    static SimpleDateFormat useableDate = new SimpleDateFormat("yyyy-MM-dd");
    static SimpleDateFormat useableTime = new SimpleDateFormat("kk:mm");

    static {
        useableDate.setLenient(false);
    }

    static {
        useableTime.setLenient(false);
    }
    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {

        Date d = new Date(150, 1, 1);
        SimpleDateFormat timeFormat = new SimpleDateFormat("kk:mm:ss");
        timeFormat.setLenient(false);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        // System.out.println("Full date-time: " + d);
        // System.out.println("Formatted Time: " + timeFormat.format(d));
        //System.out.println("Formatted date: " + dateFormat.format(d));
        //d.setYear(2000);
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        //cal.set(2020, 1, 1);
        //System.out.println(cal.get(Calendar.YEAR));
        int day = cal.get(Calendar.DAY_OF_YEAR);

        Date d2 = new Date();

        HashSet<Reason> res = new HashSet<>();
        res.add(Reason.CHECKUP);
        try {
            Appointment app = new Appointment(d, 20, "Sample Name", res);
            System.out.println(app.getName() + " TIME " + app.getDurMin());
        } catch (DataInvalidException di) {
            System.out.println("Error: " + di.getMessage());
        }
        

        while (true) {
            try {
                System.out.println("---------------------------------------------------");
                int choice = getMenuChoice();
                switch (choice) {
                    case 1:
                        System.out.println("[You chose to make an appointment]");
                        addAppointment();
                        break;
                    case 2:
                        System.out.println("[List appointments by date]");

                        break;
                    case 3:
                        System.out.println("[List appointments by Name]");

                        break;
                    case 4:
                        System.out.println("[List appointments by their first reason]");

                        break;
                    case 0:
                        System.out.println("[You chose to exit]");
                        return;
                    default:
                        System.out.println("[Invalid choice please enter 1, 2, 3, 4 or 0 to exit]");
                        break;
                }
            } catch (InputMismatchException i) {
                System.out.println("Wrong character entered please try again");
                input.nextLine();
            }

        }

    }

    private static int getMenuChoice() {
        System.out.print("Please make a choice [0-4]:\n"
                + "1. Make an appointment\n"
                + "2. List appointments by date\n"
                + "3. List appointments by Name\n"
                + "4. List appointments by their first reason\n"
                + "0. Exit\n"
                + "Your choice is: ");
        try {
            int choice = input.nextInt();
            input.nextLine();
            return choice;
        } catch (InputMismatchException e) {
            System.out.println("wrong selection please try again");
            input.nextLine();
        }
        return 5;
    }

    private static void addAppointment() {
        try{
            System.out.println("Enter appointment reason");
            String reason = input.nextLine();
            if(reason.equalsIgnoreCase("checkup") || reason.equalsIgnoreCase("referral") ||){
                System.out.println("Incorrect input please try again");
            }
        }catch (DataInvalidException di) {
            System.out.println("Error: " + di.getMessage());
        }
    }
}
