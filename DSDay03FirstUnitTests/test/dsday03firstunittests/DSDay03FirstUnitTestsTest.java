/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsday03firstunittests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author john
 */
public class DSDay03FirstUnitTestsTest {
    
    public DSDay03FirstUnitTestsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        DSDay03FirstUnitTests.main(args);
        fail("The test case is a prototype.");
    }

    @Test
    public void testMultiplyTwo() {
        System.out.println("multiplyTwo");
        int num = 10;
        int expResult = 20;
        int result = DSDay03FirstUnitTests.multiplyTwo(num);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }
    
}
