package day01miscpractice;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;

public class Day01MiscPractice {

    static SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd");

    static {
        simpleDate.setLenient(false);
    }

    public static void main(String[] args) {
        Date date = new Date(100, 0, 1);
        Date dateTwo = new Date(90, 0, 1);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.setTime(dateTwo);
        c.set(1995,0,1);
        LocalDate localD = LocalDate.of(200, 1, 1);
        LocalDate localD2 = LocalDate.of(200, 1, 20);
        Period p = Period.between (localD, localD2);
        
        System.out.println(p.getDays());
    }

}
