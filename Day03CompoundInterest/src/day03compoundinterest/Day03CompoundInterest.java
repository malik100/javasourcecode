/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03compoundinterest;

/**
 *
 * @author john
 */
public class Day03CompoundInterest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double monthlyDeposit = 100;
        double totalValue = 0;
        int monthsPassed = 3;
        for (int i = 0; i < monthsPassed; i++){
            totalValue = (monthlyDeposit + totalValue) * ((1 + 0.00417));
            System.out.printf("%d months passed  and you have %.2f \n", i+1, totalValue);
        }
        
    }
    
}
