
package day08arrays2d;


public class Day08Arrays2D {

    
    public static void main(String[] args) {
        int[][] data2D ={{1,2,3},{4,5,6}};
        int value = 2;
        int sum = 0;
        for (int row = 0; row < data2D.length; row++) {
            for (int col = 0; col < data2D[row].length; col++) {
                if(col == value){
                    sum += data2D[row][col];
                }
            }
            System.out.println();
        }
        System.out.println(sum);
    }
    
}
