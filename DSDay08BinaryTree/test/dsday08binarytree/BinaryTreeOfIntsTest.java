/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsday08binarytree;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author john
 */
public class BinaryTreeOfIntsTest {
    
    

    @Test
    public void testPut() {
        System.out.println("put");
        BinaryTreeOfInts instance = new BinaryTreeOfInts();
        instance.put(50);
        instance.put(30);
        instance.put(100);
        instance.put(75);
        instance.put(200);
        instance.put(25);
        instance.put(40);
        instance.put(10);
        instance.put(29);
        instance.put(150);
        instance.put(220);
        int expResult = 10;
        int result = instance.root.left.left.left.value;
        assertEquals(expResult, result);
        int expResult2 = 220;
        int result2 = instance.root.right.right.right.value;
        assertEquals(expResult2, result2);
        int expResult3 = 150;
        int result3 = instance.root.right.right.left.value;
        assertEquals(expResult3, result3);
        int expResult4 = 75;
        int result4 = instance.root.right.left.value;
        assertEquals(expResult4, result4);
        int expResult5 = 29;
        int result5 = instance.root.left.left.right.value;
        assertEquals(expResult5, result5);
    }

    @Test
    public void testGetSumOfAllValues() {
        System.out.println("getSumOfAllValues");
        BinaryTreeOfInts instance = new BinaryTreeOfInts();
        instance.put(50);
        instance.put(30);
        instance.put(100);
        instance.put(75);
        instance.put(200);
        instance.put(25);
        instance.put(40);
        instance.put(10);
        instance.put(29);
        instance.put(150);
        instance.put(220);
        int expResult = 929;
        int result = instance.getSumOfAllValues();
        assertEquals(expResult, result);
    }
    

    @Test
    public void testGetValuesInOrder() {
        System.out.println("getValuesInOrder");
        BinaryTreeOfInts instance = new BinaryTreeOfInts();
        instance.put(50);
        instance.put(30);
        instance.put(100);
        instance.put(75);
        instance.put(200);
        instance.put(25);
        instance.put(40);
        instance.put(10);
        instance.put(29);
        instance.put(150);
        instance.put(220);
        int[] expResult = {10,25,29,30,40,50,75,100,150,200,220};
        int[] result = instance.getValuesInOrder();
        assertArrayEquals(expResult, result);
    }
    
}
