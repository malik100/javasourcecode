
package dsday08binarytree;


public class DSDay08BinaryTree {

    
    public static void main(String[] args) {
        /*
        BinaryTreeOfInts bt = new BinaryTreeOfInts();
        bt.put(5);
        
        bt.put(3);
        bt.put(7);
        bt.put(8);
        bt.put(1);
        bt.put(4);
        bt.put(2);
        System.out.println("Nodes: " + bt.nodesCount + "\n");
        
        System.out.println("Root: " + bt.root.value);
        
        System.out.println("Root left: " + bt.root.left.value);
        System.out.println("Root right: " + bt.root.right.value + "\n");
        
        System.out.println("Root left.left: " + bt.root.left.left.value);
        System.out.println("Root left.right: " + bt.root.left.right.value);
        System.out.println("Root right.right: " + bt.root.right.right.value);
        
        System.out.println("\nRoot left.left.right: " + bt.root.left.left.right.value);
        
        System.out.println("Sum " + bt.getSumOfAllValues());
        int[] res = bt.getValuesInOrder();
        for (int i = 0; i < res.length; i++) {
            System.out.println(res[i] + " >");
        }
*/
        BinaryTreeOfInts instance = new BinaryTreeOfInts();
        instance.put(50);
        instance.put(30);
        instance.put(100);
        instance.put(75);
        instance.put(200);
        instance.put(25);
        instance.put(40);
        instance.put(10);
        instance.put(29);
        instance.put(150);
        instance.put(220);
        int[] result = instance.getValuesInOrder();
        for(int i : result){
            System.out.println("> " + i);
        }
        int[] expResult = {10,25,29,30,40,50,75,100,150,200,220};
        for(int i : expResult){
            System.out.println("> " + i);
        }
        
        for(int i : instance){
            System.out.println(">>>>>>>>>>>>   " + i);
        }
        
    }
    
}
