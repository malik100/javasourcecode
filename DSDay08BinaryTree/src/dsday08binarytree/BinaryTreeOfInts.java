package dsday08binarytree;

import java.util.Iterator;

public class BinaryTreeOfInts implements Iterable<Integer>{

    
    
    class SimplisticBinaryTreeIterator implements Iterator<Integer>{

        public SimplisticBinaryTreeIterator(int[] valuesInOrder) {
            this.valuesInOrder = valuesInOrder;
        }
        
        int[] valuesInOrder;
    int iterateIndex;
        @Override
            public boolean hasNext() {
                return iterateIndex < valuesInOrder.length;
            }

            @Override
            public Integer next() {
                return valuesInOrder[iterateIndex++];
            }
    }
    @Override
    public Iterator<Integer> iterator() {
        return new SimplisticBinaryTreeIterator(getValuesInOrder());
    }

    

    

    public class NodeOfInt {

        int value; // could also be key,value pair
        NodeOfInt left;
        NodeOfInt right;

        public NodeOfInt(int value, NodeOfInt left, NodeOfInt right) {
            this.value = value;
            this.left = left;
            this.right = right;
        }

    }

    public NodeOfInt root;
    public int nodesCount;

    // throws exception if put attempts to insert value that already exists (a duplicate)
    void put(int value) throws IllegalArgumentException {
        NodeOfInt newNode = new NodeOfInt(value, null, null);
        if (root == null) {
            root = newNode;
            nodesCount++;
            return;
        }
        NodeOfInt current = root;
        while (current != null) {
            if (value > current.value && current.right == null) {
                current.right = newNode;
                nodesCount++;
                return;
            }
            if (value < current.value && current.left == null) {
                current.left = newNode;
                nodesCount++;
                return;
            }
            current = value > current.value ? current.right : current.left;
        }
    }

    // uses compute the sum of all values in the entire tree using the recursive method below
    public int getSumOfAllValues() {
        int sum = 0;
        BinaryTreeOfInts bt = new BinaryTreeOfInts();

        sum += root.value;
        sum += bt.getSumOfThisAndSubNodes(root);

        return sum;
    }

    // private helper recursive method to implement the above method
    private int getSumOfThisAndSubNodes(NodeOfInt node) {
        int sum = 0;
        
        BinaryTreeOfInts bt = new BinaryTreeOfInts();
        if (node.left != null) {
            sum += node.left.value;
            sum += bt.getSumOfThisAndSubNodes(node.left);
        }
        if (node.right != null) {
            sum += node.right.value;
            sum += bt.getSumOfThisAndSubNodes(node.right);
        }

        return sum;
    }
    // uses recursion to collect all values from largest to smallest

    int[] getValuesInOrder() { // from largest to smallest
        resultArray = new int[nodesCount];
        resultIndex = 0;
        this.collectValuesInOrder(root);
        return resultArray;
    }
    // private helper recursive method to implement the above method
    private void collectValuesInOrder(NodeOfInt node) {
        if(node.left != null){
            this.collectValuesInOrder(node.left);
        }
        resultArray[resultIndex] = node.value;
        resultIndex++;
        if(node.right != null){
            this.collectValuesInOrder(node.right);
        }
        
        
    }
    // data structures used to make collecting values in order easier
    public int[] resultArray;
    public int resultIndex;
}

/*
NodeOfInt current = root;
        while (current != null) {
            if (value > current.value) {
                if (current.right == null) {
                    current.right = newNode;
                    nodesCount++;
                    return;
                }
                if(current.right.value < value){
                    newNode.left = current.right;
                    current.right = newNode;
                    nodesCount++;
                    return;
                }
            }
            if (value < current.value) {
                if (current.left == null) {
                    current.left = newNode;
                    nodesCount++;
                    return;
                }
                if(current.left.value < value){
                    newNode.left = current.left;
                    current.left = newNode;
                    nodesCount++;
                    return;
                }
            }
            current = value > current.value ? current.right : current.left;
        }
 */
