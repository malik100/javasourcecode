
package day04celsiustofahrenheight;

public class Day04CelsiusToFahrenheight {

    
    public static void main(String[] args) {
        
        System.out.println(celToFah(1));
        System.out.println(fahToCel(33.8));
        double newFahrenheight = 120;
        System.out.printf("%-15s %-15s %-8s %-15s %-15s \n","celsius", "fahrenheight","|", "Fahrenheight", "celsius");
        for (double celsius = 40; celsius > 0; celsius--) {
            System.out.printf("%-15.1f %-15.1f %-8s %-15.1f %-15.2f \n", celsius, celToFah(celsius), "|", newFahrenheight, fahToCel(newFahrenheight));
            newFahrenheight -= 10;
        }
    }
    public static double celToFah(double celsius){
        double fahrenheight = 0;
        fahrenheight = ( 9.0 / 5) * celsius + 32;
        
        return fahrenheight;
    }
    public static double fahToCel(double fahrenheight){
        double celsius = 0;
        celsius = ( 5.0 / 9) * (fahrenheight - 32);
        
        return celsius;
    }
}
