/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03fourpatterns;

/**
 *
 * @author john
 */
public class DAY03FourPatterns {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int input = 6;
        ///////// PATTERN D
        for (int y = 0; y < input; y++) {
            for(int x = 0; x < input; x++){
                if(y <= x){
                    System.out.print(" # ");
                }
                else{
                    System.out.print("   ");
                }
            }
            System.out.println("   ");
        }
        //////////////////// PATTERN A
        for (int y = 0; y < input; y++) {
            for(int x = 0; x < input; x++){
                if(y >= x){
                    System.out.print(" # ");
                }
                else{
                    System.out.print("   ");
                }
            }
            System.out.println("   ");
        }
        System.out.println("");
        //////////////////////// B
        for (int y = 0; y < input; y++) {
            for(int x = 0; x < input; x++){
                if(y < input - x){
                    System.out.print(" # ");
                }
                else{
                    System.out.print("   ");
                }
            }
            System.out.println("   ");
        }
        System.out.println("");
        //////////////////////// C
        for (int y = 0; y < input; y++) {
            for(int x = 0; x < input; x++){
                if(y >= input - x){
                    System.out.print(" # ");
                }
                else{
                    System.out.print("   ");
                }
            }
            System.out.println(" # ");
        }
    }
    
}
