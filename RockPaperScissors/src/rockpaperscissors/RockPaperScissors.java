/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rockpaperscissors;

import java.util.Scanner;

/**
 *
 * @author john
 */
public class RockPaperScissors {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int scissor = 0;
        int rock = 1;
        int paper = 2;
        Scanner input = new Scanner(System.in);
        System.out.println("Enter either 0 for scissor, 1 for rock or 2 for paper: ");
        int playerChoice = input.nextInt();
        if (playerChoice != 0 && playerChoice != 1 && playerChoice != 2){
            System.out.println("wrong input you can only enter 0, 1 or 2.");
            System.exit(1);
        }
        int computerChoice = (int)(Math.random() * 3);
        String[] playerSelectedName = {"scissor", "rock", "paper"};
        String[] computerSelectedName = {"scissor", "rock", "paper"};
        System.out.println("Player chose " + playerSelectedName[playerChoice]);
        System.out.println("Computer chose " + computerSelectedName[computerChoice]);
        if ((playerChoice == 0 && computerChoice == 0 ) || (playerChoice == 1 && computerChoice == 1 ) ||
                (playerChoice == 2 && computerChoice == 2 )){
            System.out.println("It was a draw");
        }
        if ((playerChoice == scissor && computerChoice == rock) || (playerChoice == rock && computerChoice == paper ) ||
               (playerChoice == paper && computerChoice == scissor )){
            System.out.println("Computer wins");
        }
        if ((playerChoice == rock && computerChoice == scissor) || (playerChoice == paper && computerChoice == rock ) ||
               (playerChoice == scissor && computerChoice == paper )){
            System.out.println("Player Wins");
        }
    }
    
}
