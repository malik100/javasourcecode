/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03learnaddition;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author john
 */
public class Day03LearnAddition {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Random numberGenerator = new Random();
        int firstNumber = numberGenerator.nextInt(100); //0-99 only
        int secondNumber = numberGenerator.nextInt(100);
        Scanner input = new Scanner(System.in);
        int numberSum = firstNumber + secondNumber;
        System.out.println("Enter a guess number which is the sum of two numbers below 100");
        int guessNumber = input.nextInt();
//        if (guessNumber == numberSum){
//            System.out.println("Correct!");
//        }
//        else{
//            System.out.printf("%d is incorrect the correct number was %d", guessNumber, numberSum);
//        }
        boolean guessValidity = numberSum == guessNumber;
        System.out.println(numberSum);
        System.out.printf("%b", guessValidity);
    }
    
}
