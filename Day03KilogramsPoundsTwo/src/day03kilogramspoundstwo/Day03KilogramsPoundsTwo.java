/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03kilogramspoundstwo;

/**
 *
 * @author john
 */
public class Day03KilogramsPoundsTwo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double kilograms = 1;
        double pounds = 1;
        double newKilograms = 1;
        double newPounds = 20;
        System.out.printf("%-12s %-12s %-6s %-12s %-12s \n", "Kilograms", "Pounds", "|", "Pounds", "Kilograms" );
        for (int i = 0; i < 99; i++){
            pounds = kilograms * 2.2;
            newKilograms = newPounds / 2.2;
            System.out.printf("%-12d %-12.1f %-6s %-12d %-12.2f \n",(int)kilograms, pounds, "|", (int)newPounds, newKilograms);
            kilograms += 2;
            newPounds += 5;
        }
    }
}
