
package day06dectohex;


public class Day06DecToHex {

    
    public static void main(String[] args) {
        int dec = 12;
        char hex;
        if (dec <= 9){
            hex = (char)('0' + dec);
            System.out.println("0-9 version:" + dec);
        }else{
            hex =  (char)('A' + dec - 10);
        }
        System.out.println("A-F version:" + hex);
    }
    
}
