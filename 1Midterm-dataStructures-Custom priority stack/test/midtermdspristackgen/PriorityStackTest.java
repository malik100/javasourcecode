/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package midtermdspristackgen;

import java.util.NoSuchElementException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author john
 */
public class PriorityStackTest {
    
        
    // to make sure generic behaviour is well implemented we use this custom class
    class Person {
        
        Person(String name, int age) {
            this.name = name;
            this.age = age;
        }
        
        private String name;
        private int age;
        
        @Override
        public String toString() {
            return name + "^" + age;
        }
        
        @Override
        public boolean equals(Object obj) {
            Person other = (Person)obj;
            return name.equals(other.name) && (age == other.age);
        }
        
    }
    
    // For full marks: write at least 4 additional tests, as described below.
    
    // Midterm TODO: To obtain full marks for the task you must write at least
    // one test for every case where code will throw NoSuchElementException (3 cases, min 3 tests)
    
    // Also: write a test for toArrayReversed() method (1 test)
    
    // NOTE: These tests are accurate to the best of teacher's knowledge.
    // However, if something doesn't seem right with a test you must consult the teacher
    // *during* the midterm, not after.
    
    
    @Test
    public void PushTest1() {
        PriorityStack<String> instance = new PriorityStack<>();
        instance.push("Jerry");
        instance.push("Terry");
        instance.push("Larry", true);
        instance.push("Barry");
        assertEquals(4,instance.getSize());
        assertEquals("[Barry:N,Larry:P,Terry:N,Jerry:N]",instance.toString());        
    }
    //*************** version of the given test with PERSON
    @Test
    public void PushTest1PERSON() {
        PriorityStack<Person> instance = new PriorityStack<>();
        instance.push(new Person("Jerry", 33));
        instance.push(new Person("Terry", 50));
        instance.push(new Person("Larry", 80), true);
        instance.push(new Person("Barry", 10));
        assertEquals(4,instance.getSize());
        assertEquals("[Barry^10:N,Larry^80:P,Terry^50:N,Jerry^33:N]",instance.toString());        
    }

    @Test
    public void PushPopTest1() {
        PriorityStack<String> instance = new PriorityStack<>();
        instance.push("Jerry");
        instance.push("Terry");
        instance.push("Larry", true);
        instance.push("Barry");
        assertEquals(4,instance.getSize());
        String res1 = instance.pop();
        assertEquals("Barry", res1);
        String res2 = instance.pop();
        assertEquals("Larry", res2);
        assertEquals("[Terry:N,Jerry:N]",instance.toString());        
    }
    
    //********************GENERIC version with PERSON
@Test
    public void PushPopTest1PERSON() {
        PriorityStack<Person> instance = new PriorityStack<>();
        instance.push(new Person("Jerry", 33));
        instance.push(new Person("Terry", 50));
        instance.push(new Person("Larry", 80), true);
        instance.push(new Person("Barry", 10));
        assertEquals(4,instance.getSize());
        Person res1 = instance.pop();
        assertEquals(new Person("Barry", 10), res1);
        Person res2 = instance.pop();
        assertEquals(new Person("Larry", 80), res2);
        assertEquals("[Terry^50:N,Jerry^33:N]",instance.toString());        
    }
    
    @Test
    public void PushPopPushTest1() {
        PriorityStack<String> instance = new PriorityStack<>();
        instance.push("Jerry");
        instance.push("Terry");
        instance.push("Larry", true);
        instance.push("Barry");
        assertEquals(4,instance.getSize());
        String res1 = instance.pop();
        assertEquals("Barry", res1);
        String res2 = instance.pop();
        assertEquals("Larry", res2);
        assertEquals(2,instance.getSize());
        assertEquals("[Terry:N,Jerry:N]",instance.toString());
        instance.push("Eva", true);
        instance.push("Martha");
        instance.push("Ruth");
        assertEquals(5,instance.getSize());
        assertEquals("[Ruth:N,Martha:N,Eva:P,Terry:N,Jerry:N]",instance.toString());
    }
    //*****PERSON version
    @Test
    public void PushPopPushTest1PERSON() {
        PriorityStack<Person> instance = new PriorityStack<>();
        instance.push(new Person("Jerry", 33));
        instance.push(new Person("Terry", 50));
        instance.push(new Person("Larry", 80), true);
        instance.push(new Person("Barry", 10));
        assertEquals(4,instance.getSize());
        Person res1 = instance.pop();
        assertEquals(new Person("Barry", 10), res1);
        Person res2 = instance.pop();
        assertEquals(new Person("Larry", 80), res2);
        assertEquals(2,instance.getSize());
        assertEquals("[Terry^50:N,Jerry^33:N]",instance.toString());
        instance.push(new Person("Eva", 30), true);
        instance.push(new Person("Martha", 100));
        instance.push(new Person("Ruth", 25));
        assertEquals(5,instance.getSize());
        assertEquals("[Ruth^25:N,Martha^100:N,Eva^30:P,Terry^50:N,Jerry^33:N]",instance.toString());
    }
    
    @Test
    public void PopPriorityTest1() {
        PriorityStack<String> instance = new PriorityStack<>();
        instance.push("Jerry");
        instance.push("Terry");
        instance.push("Larry", true);
        instance.push("Barry");
        assertEquals(4,instance.getSize());
        String res1 = instance.popPriority();
        assertEquals("Larry", res1);
        assertEquals(3,instance.getSize());        
        String res2 = instance.popPriority();
        assertEquals("Barry", res2);
        assertEquals(2,instance.getSize());
        assertEquals("[Terry:N,Jerry:N]",instance.toString());
    }
    
    //PERSON Version-------------------------------------------------------
    @Test
    public void PopPriorityTest1PERSON() {
        PriorityStack<Person> instance = new PriorityStack<>();
        instance.push(new Person("Jerry", 33));
        instance.push(new Person("Terry", 50));
        instance.push(new Person("Larry", 80), true);
        instance.push(new Person("Barry", 10));
        assertEquals(4,instance.getSize());
        Person res1 = instance.popPriority();
        assertEquals(new Person("Larry", 80), res1);
        assertEquals(3,instance.getSize());        
        Person res2 = instance.popPriority();
        assertEquals(new Person("Barry", 10), res2);
        assertEquals(2,instance.getSize());
        assertEquals("[Terry^50:N,Jerry^33:N]",instance.toString());
    }
    
    @Test
    public void PopPriorityTest2FromTop() {
        PriorityStack<String> instance = new PriorityStack<>();
        instance.push("Jerry");
        instance.push("Terry");
        instance.push("Barry");
        instance.push("Larry", true);
        assertEquals(4,instance.getSize());
        String res1 = instance.popPriority();
        assertEquals("[Barry:N,Terry:N,Jerry:N]",instance.toString());
    }
    
    //PERSON Version-------------------------------------------------------
    @Test
    public void PopPriorityTest2FromTopPERSON() {
        PriorityStack<Person> instance = new PriorityStack<>();
        instance.push(new Person("Jerry", 33));
        instance.push(new Person("Terry", 50));
        instance.push(new Person("Barry", 10));
        instance.push(new Person("Larry", 80), true);
        assertEquals(4,instance.getSize());
        Person res1 = instance.popPriority();
        assertEquals("[Barry^10:N,Terry^50:N,Jerry^33:N]",instance.toString());
    }
    
    @Test
    public void HasValueTest1() {
        PriorityStack<String> instance = new PriorityStack<>();
        instance.push("Jerry");
        instance.push("Terry");
        instance.push("Larry", true);
        instance.push("Barry");
        assertEquals(4,instance.getSize());
        assertEquals("[Barry:N,Larry:P,Terry:N,Jerry:N]",instance.toString());
        int res1 = instance.hasValue("Eva");
        assertEquals(-1, res1);
        int res2 = instance.hasValue("Terry");
        assertEquals(2, res2);
    }
    //PERSON version--------------------
    @Test
    public void HasValueTest1PERSON() {
        PriorityStack<Person> instance = new PriorityStack<>();
        instance.push(new Person("Jerry", 33));
        instance.push(new Person("Terry", 50));
        instance.push(new Person("Larry", 80), true);
        instance.push(new Person("Barry", 10));
        assertEquals(4,instance.getSize());
        assertEquals("[Barry^10:N,Larry^80:P,Terry^50:N,Jerry^33:N]",instance.toString());
        int res1 = instance.hasValue(new Person("Eva", 30));
        assertEquals(-1, res1);
        int res2 = instance.hasValue(new Person ("Terry", 50));
        assertEquals(2, res2);
    }
    
    @Test
    public void HasValueRemoveValueTest1() {
        PriorityStack<String> instance = new PriorityStack<>();
        instance.push("Jerry");
        instance.push("Terry");
        instance.push("Larry", true);
        instance.push("Barry");
        assertEquals(4,instance.getSize());
        assertEquals("[Barry:N,Larry:P,Terry:N,Jerry:N]",instance.toString());
        int res1 = instance.hasValue("Eva");
        assertEquals(-1, res1);
        int res2 = instance.hasValue("Terry");
        assertEquals(2, res2);
        String res3 = instance.removeValue("Jerry");
        assertEquals("Jerry", res3);
        String res4 = instance.removeValue("Barry");
        assertEquals("Barry", res4);
        String res5 = instance.pop();
        assertEquals("Larry", res5);
        String res6 = instance.pop();
        assertEquals("Terry", res6);
        assertEquals("[]", instance.toString());
        assertEquals(0, instance.getSize());
    }
    //PERSON version-----------------------
    @Test
    public void HasValueRemoveValueTest1PERSON() {
        PriorityStack<Person> instance = new PriorityStack<>();
        instance.push(new Person("Jerry", 33));
        instance.push(new Person("Terry", 50));
        instance.push(new Person("Larry", 80), true);
        instance.push(new Person("Barry", 10));
        assertEquals(4,instance.getSize());
        assertEquals("[Barry^10:N,Larry^80:P,Terry^50:N,Jerry^33:N]",instance.toString());
        int res1 = instance.hasValue(new Person ("Eva", 30));
        assertEquals(-1, res1);
        int res2 = instance.hasValue(new Person("Terry", 50));
        assertEquals(2, res2);
        Person res3 = instance.removeValue(new Person("Jerry", 33));
        assertEquals(new Person("Jerry", 33), res3);
        Person res4 = instance.removeValue(new Person("Barry", 10));
        assertEquals(new Person("Barry", 10), res4);
        Person res5 = instance.pop();
        assertEquals(new Person("Larry", 80), res5);
        Person res6 = instance.pop();
        assertEquals(new Person("Terry", 50), res6);
        assertEquals("[]", instance.toString());
        assertEquals(0, instance.getSize());
    }
    
    
    @Test
    public void ReorderByPriorityTest1() {
        PriorityStack<String> instance = new PriorityStack<>();
        instance.push("Jerry", false);
        instance.push("Terry");
        instance.push("Larry", true);
        instance.push("Barry");
        instance.push("Eva", true);
        instance.push("Martha");
        instance.push("Ruth");
        assertEquals(7,instance.getSize());
        instance.reorderByPriority();
        assertEquals("[Eva:P,Larry:P,Ruth:N,Martha:N,Barry:N,Terry:N,Jerry:N]",instance.toString());
        String res1 = instance.pop();
        assertEquals("Eva", res1);
        String res2 = instance.pop();
        assertEquals("Larry", res2);
        String res3 = instance.pop();
        assertEquals("Ruth", res3);
        assertEquals(4,instance.getSize());
        assertEquals("[Martha:N,Barry:N,Terry:N,Jerry:N]",instance.toString());
    }
    
//PERSON version
     @Test
    public void ReorderByPriorityTest1PERSON() {
        PriorityStack<Person> instance = new PriorityStack<>();
        instance.push(new Person("Jerry", 33), false);
        instance.push(new Person("Terry", 50));
        instance.push(new Person("Larry", 100), true);
        instance.push(new Person("Barry", 40));
        instance.push(new Person("Eva", 20), true);
        instance.push(new Person("Martha", 35));
        instance.push(new Person ("Ruth", 80));
        assertEquals(7,instance.getSize());
        instance.reorderByPriority();
        assertEquals("[Eva^20:P,Larry^100:P,Ruth^80:N,Martha^35:N,Barry^40:N,Terry^50:N,Jerry^33:N]"
                ,instance.toString());
        Person res1 = instance.pop();
        assertEquals(new Person("Eva", 20), res1);
        Person res2 = instance.pop();
        assertEquals(new Person("Larry", 100), res2);
        Person res3 = instance.pop();
        assertEquals(new Person("Ruth", 80), res3);
        assertEquals(4,instance.getSize());
        assertEquals("[Martha^35:N,Barry^40:N,Terry^50:N,Jerry^33:N]",instance.toString());
    }
    
    
    @Test
    public void CustomEqualsTest1() {
        PriorityStack<Person> instance = new PriorityStack<>();
        instance.push(new Person("Jerry", 33));
        Person p1 = new Person("Maria", 22);
        instance.push(p1, true);
        instance.push(new Person("Tom", 44));
        instance.push(new Person("Eva", 55));
        assertEquals("[Eva^55:N,Tom^44:N,Maria^22:P,Jerry^33:N]", instance.toString());
        int res1 = instance.hasValue(new Person("Tom", 44));
        assertEquals(1, res1);
        Person res2 = instance.removeValue(new Person("Maria", 22));
        assertEquals(p1, res2); // this checks if p1.equals(res2) return true
        assertEquals(true, p1 == res2); // must return exactly the object that was originally pushed
    }

    // REMIDER: If you implemented toArray() but not toArrayReversed() then
	// you must write your own unit test for that method or modify this one below
    @Test
    public void ReverseTest1() {
        PriorityStack<String> instance = new PriorityStack<>();
        instance.push("Jerry");
        instance.push("Terry");
        instance.push("Larry");
        instance.push("Barry");
        Object [] result = instance.toArrayReversed(new String[10]);
        Object [] expected = {"Jerry", "Terry", "Larry", "Barry" };
        Assert.assertArrayEquals(expected, result);
    }
    
    // Custom test to NoSuchElementException if value not found for "removeValue" method
    @Test (expected = NoSuchElementException.class)
    public void exceptionsTestOne(){
        PriorityStack<String> instance = new PriorityStack<>();
        instance.push("Jerry");
        instance.push("Terry");
        instance.push("Larry");
        instance.push("Barry");
        instance.removeValue("abcde");  //called here
    }
    
    // Custom test to NoSuchElementException for pop() method
    @Test (expected = NoSuchElementException.class)
    public void exceptionsTestTwo(){
        PriorityStack<String> instance = new PriorityStack<>();
        instance.push("Jerry");
        instance.push("Terry");
        instance.pop();
        instance.pop();
        instance.pop();  //exception expected here
    }
    
    // Custom test to NoSuchElementException for popPriority() method
    @Test (expected = NoSuchElementException.class)
    public void exceptionsTestThree(){
        PriorityStack<String> instance = new PriorityStack<>();
        instance.push("Joe");
        instance.push("Jerry");
        instance.push("Terry", true);
        instance.popPriority();
        instance.popPriority();
        instance.popPriority();
        instance.popPriority();  //exception expected here
    }
    
    
    //custom test to see various portions of reverse array method
    @Test
    public void customReversedArrayTest(){
        PriorityStack<String> instance = new PriorityStack<>();
        instance.push("Jerry");
        instance.push("Terry");
        instance.push("Larry");
        instance.push("Barry");
        Object [] result = instance.toArrayReversed(new String[10]);
        int length = result.length;
        assertEquals(4, length);   //checking if correct length is created
        //Object[] retrieved = (Object[]) result[2];
        //String expected = "Larry";
        //assertEquals(retrieved, expected);
    }
}
