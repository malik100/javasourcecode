
package midtermdspristackgen;


public class MidtermDSPriStackGen {

    
    public static void main(String[] args) {
        //Debug and test only
        
        PriorityStack stack = new PriorityStack();
        stack.push("first",true);
        stack.push("second");
        stack.push("third",true );
        stack.push("fourth");
        stack.push("fifth");
        stack.push("sixth", true);
        System.out.println("xxxx" + stack.removeValue("sixth"));
        //stack.push(12);
        //stack.reorderByPriority();
        System.out.println(stack);
        System.out.println(stack.getSize());
        System.out.println("aaa" + stack.popPriority());
        
        String[] arrString = new String[10];
        arrString = (String[]) stack.toArrayReversed(arrString);
        for (int i = 0; i < arrString.length; i++) {
            System.out.print(arrString[i] + ",");
        }
        System.out.println("\n" + stack);
    }
    
}
