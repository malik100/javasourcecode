package midtermdspristackgen;

import java.util.NoSuchElementException;

public class PriorityStack<T> {

    private class Container<T> {

        T value;
        boolean hasPriority;
        Container<T> nextBelow;

        //constructor for container
        public Container(T value, boolean hasPriority, Container<T> nextBelow) {
            this.value = value;
            this.hasPriority = hasPriority;
            this.nextBelow = nextBelow;
        }

    }

    private Container<T> top; // top of the stack element

    private int size;

    public void push(T value) {
        push(value, false); //default false priority push
    }

    public void push(T value, boolean hasPriority) {
        Container newCont = new Container(value, hasPriority, null);
        if (size == 0) {
            top = newCont;  //if first item added
        } else {
            newCont.nextBelow = top;  //added to existing items
            top = newCont;
        }
        size++;
    }

    public T pop() {
        // remove and return the top item
        // if no item found (size == 0) then throw NoSuchElementException
        if (size == 0) {
            throw new NoSuchElementException();
        }
        T returnVal = top.value;
        top = top.nextBelow;
        size--;
        return returnVal;
    }

    public T popPriority() {
        // find item with priority starting from the top, remove it and return it
        // if no priority item found then remove and return the top item
        // if stack is empty then throw NoSuchElementException
        if(size == 0){
            throw new NoSuchElementException();
        }
        if(top.hasPriority){  //if first value has priority
            return pop();
        }
        Container before = top;
        for (int i = 0; i < size; i++) {
            if(before.nextBelow == null){ break;} //avoid null pointer
            if (!before.nextBelow.hasPriority) {
                before = before.nextBelow;      //continue if no priority
            } else {
                T foundVal = (T) before.nextBelow.value; //if priority found
                before.nextBelow = before.nextBelow.nextBelow; //delete reference to old vlaue
                size--;
                return foundVal;
            }
        }
        return pop(); //if nothing found return top using pop() method
    }
    
    public int hasValue(T value) {
        Container current = top;
        for (int i = 0; i < size; i++) {
            if(current == null){break;}; //avoid null value check
            if(value.equals(current.value)){
                return i;               //return the found index
            }
            current = current.nextBelow;
        }
        return -1;
    }

    public T removeValue(T value) {
        // removes the first item from top containing the value and returns the value
        // if item with value is not found throw NoSuchElementException
        int index = hasValue(value);    //calls has value method to check first
        Container before = top;
        for (int i = 0; i < index -1; i++) {    //iterating to find item just before the value
            before = before.nextBelow;
        }
        if(index == -1){
            throw new NoSuchElementException();
        }
        T removedVal;
        if(index == 0){     //value to remove is first item
            removedVal = top.value;
            top = before.nextBelow;
        }else{
            removedVal = (T) before.nextBelow.value;
            before.nextBelow = before.nextBelow.nextBelow;
        }
        size--;
        return removedVal;
    }
    
    public int getSize() {
        return size;
    }
    
    public void reorderByPriority() {
        // reorder items (re-create a new stack, if you like)
        // where all priority items are on top and non-priority items are below them
        // Note: order within the priority items group and non-priority items group must remain the same
        // Suggestion: instead of reordering the existing stack items
        // it may be easier to re-create a new stack with items in the order you need
        
        PriorityStack priority = new PriorityStack(); //stack that holds priority items;
        Container current = top;
        int priorityCounter = 0;    //keep track of all priority items removed
        for (int i = 0; i < size; i++) {   
            if(current.hasPriority){        //removes only priority items
                priorityCounter++;      
            }
            if(current.nextBelow != null){current = current.nextBelow;}
        }
        for (int i = 0; i < priorityCounter; i++) { //add priority items to a new stack
            T poppedItem = this.popPriority();
            priority.push(poppedItem);
        }
        for (int i = 0; i < priorityCounter; i++) { //add items back to original
            this.push((T) priority.pop(), true);
        }
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        for (Container current = top; current != null; current = current.nextBelow) {
            sb.append(current == top ? current.value : "," + current.value);
            sb.append(current.hasPriority ? ":P" : ":N");
        }
        sb.append("]");
        return sb.toString();
        // return string describing the contents of the stack, starting from the top
        // Use value.toString() to conver values kept in the stack to strings.
        // Format exactly like this (assuming T is a string to keep it simple):
        // "[Jerry:N,Terry:N,Martha:P,Tom:P,Jimmy:N]" 
        // N means item has no priority, P means item has priority
        // For full marks you must use StringBuilder, no + (string concatenation) allowed.
    }
    
    	// you may need these fields to implement toArrayReversed
    private T[] reversed;
    private int reversedCount;

    public T[] toArrayReversed(String[] template) { // Note: this is "the twist"
        // return array with items on the stack
        // WARNING: element 0 of the array must be the BOTTOM of the stack
        // NOTE: To obtain full marks for this method you must use recursion.
        // Collect items on your way back, just before returning.
        // This case is similar to when constructors of parent classes are called (Programming II course).
        T[] arr = (T[]) new String[size];
        PriorityStack tempStack = new PriorityStack();
        Container current = top;
        for (int i = 0; i < size; i++) {
            tempStack.push(current.value);  //putting values in new stack
            current = current.nextBelow;
        }
        Container newCurr = tempStack.top;
        for (int i = 0; i < tempStack.size; i++) {
            arr[i] = (T) newCurr.value;  //putting values in array now reversed
            newCurr = newCurr.nextBelow;
        }
        return arr;
    }
}
