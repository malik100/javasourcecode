
package day10students;


public class Student {
    int id;
    String name;
    byte[] image;

    public Student(int id, String name, byte[] image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

    @Override
    public String toString() {
        return String.format("%d: %s", id, name);
    }
    
}
