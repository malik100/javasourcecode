package day10students;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Database {

    private static final String dbURL = "removed for security";
    private static final String username = "removed for security";
    private static final String password = "removed for security";

    private Connection conn;

    public Database() throws SQLException {
        conn = DriverManager.getConnection(dbURL, username, password);
    }

    public ArrayList<Student> getAllStudents() throws SQLException {
        ArrayList<Student> list = new ArrayList<>();
        String sql = "SELECT id, name from students";
        PreparedStatement statement = conn.prepareStatement(sql);
        try (ResultSet result = statement.executeQuery(sql)) {
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                //System.out.printf("%d: %s is %d y/o\n", id, name, age);
                list.add(new Student(id, name, null));
            }
        }
        return list;
    }

    public Student getStudentById(int id) throws SQLException {
        PreparedStatement stmtSelect = conn.prepareStatement("SELECT * from students where id=?");
        stmtSelect.setInt(1, id);
        try (ResultSet resultSet = stmtSelect.executeQuery()) {
            if (resultSet.next()) {
                String name = resultSet.getString("name");
                byte[] image = resultSet.getBytes("image");
                return new Student(id, name, image);
            } else {
                throw new SQLException("Record not found");
            }
        }
    }

    public void addStudent(Student student) throws SQLException {
        String sql = ("insert into students values (null, ?, ?)");
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, student.name);
        statement.setBytes(2, student.image);
        statement.executeUpdate();  //for insert update delete
        System.out.println("Record inserted");
    }

//    public void updatePerson(Student student) throws SQLException {
//        String sql = "update people set name=?, age=? where id=?";
//        PreparedStatement statement = conn.prepareStatement(sql);
//        statement.setString(1, student.name);
//        statement.setInt(2, student.age);
//        statement.setInt(3, student.id);
//        statement.executeUpdate();
//        System.out.println("Record updated id =" + student.id);
//    }
}
