
package javabasics;

public class Book {
   private String title;
   private String author;
   private boolean isBorrowed;
   
   public Book(String title, String author){
       this.author = author;
       this.title = title;
  }
  
   public void borrowBook(){
       this.isBorrowed = true;
   }
   
   public void returnBook(){
       this.isBorrowed = false;
   }
   
   public boolean isBookBorrowed(){
       return isBorrowed;
   }
}

