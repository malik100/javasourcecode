package javabasics;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.InputMismatchException;

import java.util.Scanner;

public class JavaBasics {

    public static int doubleNumber(int number) {
        int result = number * 2;
        return result;
    }

    public static void main(String[] args) {
        int num = 4;
        for (int y = 1; y <= num; y++) {
            for (int x = 0; x < y; x++) {
                System.out.print("*");
            }
            System.out.println();
        }
//        Scanner input = new Scanner(System.in);
//        while (true) {
//            System.out.println("Enter a postal code");
//            String entry = input.nextLine();
//            if (!entry.matches("^[A-Za-z0-9(),'\". -]+$")) {
//                System.out.println("!!!!!!!!!Incorrect entry please try again");
//                continue;
//            }
//        if(!entry.matches("^[a-z][1-9][a-z] ?[1-9][a-z][1-9]$")){
//            System.out.println("!!!!!!!!!Incorrect entry please try again");
//            continue;
//        }
//            System.out.println("-------------------");
//            System.out.println("Entry was: " + entry);
//        }

//        try (Scanner read = new Scanner(new File("movies.txt"))) {
//
//            String movie = "";
//            int fileLines = 0;
//            String[] movieList = new String[10];
//            while (read.hasNextLine()) {
//                movie = read.nextLine();
//                movieList[fileLines] = movie;
//                fileLines++;
//            }
//
//            int set = (int) ((Math.random()) * fileLines);
//
//            movie = movieList[set];
//            System.out.println("movie: " + movie);
//            int letters = 0;
//            int words = movie.split(" ").length;
//            int tries = 1;
//            String[] progress = new String[movie.length()];
//            Scanner input = new Scanner(System.in);
//
//            for (int i = 0; i < movie.length(); i++) {
//                progress[i] = "  ";
//                if (movie.charAt(i) != ' ') {
//                    letters++;
//                    progress[i] = " _";
//                }
//            }
//
//            while (tries <= 10) {
//                if (tries > 10) {
//                    System.out.println("You ran out of tries");
//                    break;
//                }
//                System.out.println("[TRY#" + tries + "]");
//                System.out.println("Guess the movie name! " + "The movie has " + words + " words and " + letters + " letters");
//                System.out.println("----------------------------------------------------------------------------------------");
//                String prog;
//                StringBuilder s = new StringBuilder();
//                for (int i = 0; i < progress.length; i++) {
//                    s.append(progress[i]);
//                }
//                System.out.println("Current Progress:  " + s);
//                System.out.println("----------------------------------------------------------------------------------------");
//                String guess = input.nextLine();
//                if (guess.length() > 1 || guess.equals(" ")) {
//                    System.out.println("You entered too many letters or not enough ltters try again");
//                    tries++;
//                    continue;
//                }
//                for (int i = 0; i < movie.length(); i++) {
//                    if (guess.equalsIgnoreCase(String.valueOf(movie.charAt(i)))) {
//                        System.out.println("A letter matched!");
//                        progress[i] = " " + guess.toUpperCase();
//                    }
//                }
//                tries++;
//            }
//        } catch (FileNotFoundException f) {
//            System.out.println("The file was not found");
//        }
        //*********************************************************************************
//        try (Scanner fileScanner = new Scanner(new File ("inputsample.txt"))){
//        
//        int wordcount = 0;
//        int counter = 0;
//           while(fileScanner.hasNextLine()){
//               
//               String line = fileScanner.nextLine();
//               wordcount += line.split(" ").length;
//               System.out.println(wordcount);
//            }
//        
//        }catch(FileNotFoundException f){
//                    System.out.println("The file was not found");
//                } 
//         catch(InputMismatchException i){
//             System.out.println("Error Wrong Data Type");
//         }
    }
}
