/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabasics;

/**
 *
 * @author john
 */
public class ContactsManager {
    Contact[] myFriends;
    int friendsCount;
    
    ContactsManager(){
        this.friendsCount = 0;
        this.myFriends = new Contact[500];
    }
    
    public void addContact(Contact contact){
        myFriends[friendsCount] = contact;
        friendsCount++;
    }
    
    public Contact searchContact(String searchName){
        
        for (int i = 0; i < friendsCount; i++) {
            if(myFriends[i].name.equals(searchName))
                return myFriends[i];
        }
        return null;
    }
}
