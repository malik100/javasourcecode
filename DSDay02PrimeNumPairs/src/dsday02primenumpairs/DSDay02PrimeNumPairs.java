package dsday02primenumpairs;

public class DSDay02PrimeNumPairs {

    public static void main(String[] args) {
        //int[][] arr = new int[][]{{8, 5, 7, 9}, 
        //                          {4, 6, 2, 14}};
        int[][] arr = new int[][] {{1,2},{3,4}};
        SimplePrimePairs(arr);
    }

    public static void SimplePrimePairs(int[][] arr) {
        int[][]record = new int[2][10];
        int recIndex = 0;
        //Outer-----------------------------------------------------------------
        for (int rowA = 0; rowA < arr.length; rowA++) {
            for (int colA = 0; colA < arr[rowA].length; colA++) {
                //Inner-----------------------------------------------------------------  
                for (int rowB = rowA; rowB < arr.length; rowB++) {
                    int startCol = rowA == rowB ? colA : 0 ;
                    for (int colB = startCol; colB < arr[rowA].length; colB++) {
                        int sum = arr[rowA][colA] + arr[rowB][colB];
                        
                        if(rowA == rowB && colA == colB){
                            continue;
                        }
                        
                        /*
                        if(!VerifyNonDuplicate(record, rowB, colB)){
                            continue;
                        }
*/
                        if(IsPrime(sum)){
                            System.out.printf("Prime sum %d at [%d][%d] val %d "
                                    + "and [%d][%d] val %d\n", sum, rowA, colA, arr[rowA][colA], 
                                    rowB, colB, arr[rowB][colB]);
                            record[0][recIndex] = rowA;
                            record[1][recIndex] = colA;
                        }
                    }
                }
            }
        }
    }

    public static boolean VerifyNonDuplicate(int[][] record ,int rowA,int colA){
        for (int row = 0; row < 1; row++) {
            for (int col = 0; col < record[0].length; col++) {
                if(record[row][col] == colA && record[row + 1][col] == colA){
                    return false;
                }
            }
        }
        return true;
    }
    public static boolean IsPrime(int num) {
        if (num <= 1) {
            return false;
        }
        for (int divisor = 2; divisor <= num / 2; divisor++) {
            if (num % divisor == 0) {
                return false;
            }
        }
        return true;
    }
}
