/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package textiotypes;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

/**
 *
 * @author john
 */
public class TextIOTypes {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        ///INPUTSTREAM READER
        // BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("test.txt")))
        // FILEREADER
        // BufferedReader br = new BufferedReader(new FileReader(test.txt))
        
//        File file = new File ("INTEST.txt");
//        System.out.println(file.exists());
//        System.out.println(System.getProperty("user.dir"));

        FileReader fr = new FileReader("INTEST.txt");  //CREATE FILEREADER 
        BufferedReader br = new BufferedReader(fr);    //PUT FILEREADER IN BUFFER
        String currentLine = br.readLine();            //READ ONE LINE OF (BUFFERED)FILERADER
          
            while (currentLine == null){
                System.out.println(currentLine);
                currentLine = br.readLine();
            }
            br.close();
            fr.close();
            
            String[] split = currentLine.split(",");
            System.out.println(split[0] + " " + split[1] + "\n"
            + " Slary: " + split[3] + "\n Position: " + split[2]);
            
        FileWriter fw = new FileWriter("OUTTEST.txt");  //CREATE FILEWRITER
        BufferedWriter bw = new BufferedWriter(fw);     //PLACE IN BUFFERED WRITER
        
        String message = "Hello!!\n";
        bw.write(message);
        bw.close();
        fw.close();
    }
    
}
