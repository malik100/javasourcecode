/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homeworkassignments;

import java.util.Scanner;

/**
 *
 * @author john
 */
public class HomeworkAssignments {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //////BMI COMPUTATION/////////////////////////////////////////////////////////////////
        final int INCHES_IN_FOOT = 12; // constant
        
        Scanner input = new Scanner(System.in);
        System.out.print("Enter weight in pounds: ");
        int lbs = input.nextInt();
        System.out.print("Enter feet: ");
        int feet = input.nextInt();
        System.out.print("Enter inches: ");
        int inches = input.nextInt();
        // TODO: verify inputs
        int inchesTotal = inches + feet*INCHES_IN_FOOT;
        double bmi = (double)lbs / ( inchesTotal * inchesTotal ) * 703;
        int bmiInt = (int) bmi; // cast
        System.out.printf("BMI is %.2f, as an int is %d\n", bmi, bmiInt);
        // interpret the result
        if (bmi < 18.5) {
            System.out.println("Underweight");
        } else if (bmi < 25) {
            System.out.println("Healthy");
        } else if (bmi < 30) {
            System.out.println("Overweight");
        } else { // 30 or above
            System.out.println("Obese");
        }
        
        ////////////////////////////////////////////////////////////////////////////////////
        //////COMPUND INTEREST/////////////////////////////////////////////////////////////
        
        final double YEARLY_RATE_PERC = 5.0;
        
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the monthly saving amount: ");
        double monthlyAmount = input.nextDouble();
        System.out.print("How many months will you be saving for? ");
        int months = input.nextInt();
        // TODO: verify inputs
        double monthlyMultRate = YEARLY_RATE_PERC / 12 / 100;
        double money = monthlyAmount;        
        for (int i = 0; i < months; i++) {
            System.out.printf("Amount of money at month %d is %.2f\n", i+1, money);
            money *= 1.0 + monthlyMultRate; // e.g. 2% monthly is 1.02
            money += monthlyAmount;
        }
        System.out.printf("In the end I have %.2f\n", money);
        
        ////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////FRIENDS LOOP///////////////////////////////////////////////////
        
        Scanner input = new Scanner(System.in);
        {
            String name;
            int count = 0;
            while (true) {
                System.out.print("Enter friend's name, empty to finish: ");
                name = input.nextLine();
                if (name.isEmpty()) {
                    break;
                }
                System.out.printf("%s is your friend\n", name);
                count++;
            }
            String plural = count == 1 ? "" : "s";
            System.out.printf("You have %d friend%s\n", count, plural);
            
        }
        
        
        /* { // using do-while loop causes some code duplication
            String name;
            int count = 0;
            do {
                System.out.print("Enter friend's name, empty to finish: ");
                name = input.nextLine();
                if (!name.equals("")) {
                    System.out.printf("%s is your friend\n", name);
                    count++;
                }
            } while (!name.equals(""));
            String plural = count == 1 ? "" : "s";
            System.out.printf("You have %d friend%s\n", count, plural);
        } */
        ///////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////GENERAL NESTED LOOP//////////////////////////////////
        // print out 0-99 in 10 lines, 10 numbers per line
        int n = 1;
        for (int i = 0; i < 10; i++) {
            // System.out.printf(" i=%d ", i);
            for (int j = 0; j < 10; j++) {
                String separator = j == 9 ? "" : ", ";
                System.out.printf("%3d%s", n, separator);
                n++;
            }
            System.out.println();
        }
        //////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////LETTER GRADE TO GPA SWITCH////////////////////////////////
        // convert letter grade to GPA
        Scanner input = new Scanner(System.in);
        System.out.print("Enter your letter grade: ");
        String letterGrade = input.nextLine();
        double gpa = 0;
        
        switch (letterGrade) {
            case "A":
                gpa = 4.0;
                break;
            case "A-":
                gpa = 3.7;
                break;
            case "B+":
                gpa = 3.3;
                break;
            case "B":
                gpa = 3.0;
                break;
            case "B-":
                gpa = 2.7;
                break;
            case "C+":
                gpa = 2.3;
                break;
            case "C":
                gpa = 2.0;
                break;
            case "C-":
                gpa = 1.7;
                break;
            case "D":
                gpa = 1;
                break;                
            case "F":
                gpa = 0;
                break;
            default:
                System.out.println("Error: invalid grade " + letterGrade);
                System.exit(1);
        }
        System.out.printf("GPA for %s is %.1f\n", letterGrade, gpa);
        
        /*
        if (letterGrade.equals("A")) {
            gpa = 4.0;
        } else if (letterGrade.equals("A-")) {
            gpa = 3.7;
        } else if (letterGrade.equals("B+")) {
            gpa = 3.3;
        } else if (letterGrade.equals("B")) {
            gpa = 3.0;
        } else if (letterGrade.equals("B-")) {
            gpa = 2.7;
        } else if (letterGrade.equals("C+")) {
            gpa = 2.3;
        } else if (letterGrade.equals("C")) {
            gpa = 2;
        } else if (letterGrade.equals("C-")) {
            gpa = 1.7;
        } else if (letterGrade.equals("D")) {
            gpa = 1;
        } else if (letterGrade.equals("F")) {
            gpa = 0;
        } else { // invalid
            System.out.println("Error: invalid grade " + letterGrade);
            System.exit(1);
        }
        System.out.printf("GPA for %s is %.1f\n", letterGrade, gpa);
        */
        /////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////INT TO CHAR////////////////////////////////////////////////////////
        Scanner input = new Scanner(System.in);
        System.out.print("Enter an ASCII code: ");
        int charInt = input.nextInt();
        if (charInt < 0 || charInt > 127) {
            System.out.println("Error: character must be 0-127 value");
            System.exit(1);
        }
        // Version A
        System.out.println("The character is "  + (char)charInt);
        // Version B;
        char c = (char)charInt;
        System.out.printf("The character is %c\n", c);
        // Version B+
        System.out.printf("The character is %c\n", (char)charInt);
        /////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////RANDOM CHAR//////////////////////////////////////////////////////
        // similar to: random.nextInt(26)+65
        int val = (int)(Math.random()*26) + 65;
        System.out.printf("Uppercase char: %c\n", (char)val);
        /////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////DECIMAL TO HEXADECIMAL VALUE////////////////////////////////
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a decimal value (0-15): ");
        int value = input.nextInt();
        if (value < 0 || value > 15) {
            System.out.println("Error: invalid input");
            System.exit(1);
        }
        char c;
        if (value < 10) {
            c = (char)('0' + value);
        } else {
            c = (char)('A' + value - 10);
        }
        System.out.println("Hexadecimal: " + c);
        //////////////////////////////////////////////////////////////////////////////////////
        ///////////////////ADD GAME///////////////////////////////////////////////////////////
        final int MAX_RAND = 100;
        Scanner input = new Scanner(System.in);
        // generate random values 0 to MAX_RAND
        Random random = new Random();
        int n1 = random.nextInt(MAX_RAND+1);
        int n2 = random.nextInt(MAX_RAND+1);
        //
        System.out.printf("How much is %d+%d ? ", n1, n2);
        int sum = input.nextInt();
        if (sum == n1+n2) {
            System.out.println("Correct");
        } else {
            System.out.println("Invalid, the answer was " + (n1 + n2));
        }
        //////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////SORT INTEGERS////////////////////////////////////////////////
        Scanner input = new Scanner(System.in);
        System.out.print("Enter value 1: ");
        int v1 = input.nextInt();
        System.out.print("Enter value 2: ");
        int v2 = input.nextInt();
        System.out.print("Enter value 3: ");
        int v3 = input.nextInt();
        // v1 v2 v3, v1 v3 v2, v2 v3 v1, v2 v1 v3, v3 v1 v2, v3 v2 v1
        if (v1 > v2) {  
            if (v3 > v1) {
                System.out.printf("%d, %d, %d", v2, v1, v3);
            } else { // v3 < v1
                if (v3 > v2) {
                    System.out.printf("%d, %d, %d", v2, v3, v1);
                } else { // v3 < v2
                    System.out.printf("%d, %d, %d", v3, v2, v1);
                }
            }
        } else { // v1 < v2
            if (v3 > v2) {
                System.out.printf("%d, %d, %d", v1, v2, v3);
            } else { // v3 < v2
                if (v1 > v3) {
                    System.out.printf("%d, %d, %d", v3, v1, v2);
                } else { // v3 < v2
                    System.out.printf("%d, %d, %d", v1, v3, v2);
                }
            }
        }
        //////////////////////////////////////////////////////////////////////////////////
        ///////////////////SORT INTS TESTING////////////////////////////////////////////
        final int REPEAT = 20;
        Random random = new Random();
        for (int i = 0; i < REPEAT; i++) {
            int v1 = random.nextInt(10);
            int v2 = random.nextInt(10);
            int v3 = random.nextInt(10);

            // v1 v2 v3, v1 v3 v2, v2 v3 v1, v2 v1 v3, v3 v1 v2, v3 v2 v1
            if (v1 > v2) {
                if (v3 > v1) {
                    System.out.printf("%d, %d, %d", v2, v1, v3);
                } else { // v3 < v1
                    if (v3 > v2) {
                        System.out.printf("%d, %d, %d", v2, v3, v1);
                    } else { // v3 < v2
                        System.out.printf("%d, %d, %d", v3, v2, v1);
                    }
                }
            } else { // v1 < v2
                if (v3 > v2) {
                    System.out.printf("%d, %d, %d", v1, v2, v3);
                } else { // v3 < v2
                    if (v1 > v3) {
                        System.out.printf("%d, %d, %d", v3, v1, v2);
                    } else { // v3 < v2
                        System.out.printf("%d, %d, %d", v1, v3, v2);
                    }
                }
            }
            System.out.println();
        }
        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////COMPUTE STATS/////////////////////////////////////////////
        Scanner input = new Scanner(System.in);
        while (true) {
            System.out.print("Enter an integer, 0 to end input: ");
            int value = input.nextInt();
            if (value == 0) {
                break;
            }
            // compute statistics here
            
        }
        // display statistics here
        
        System.out.println("Done.");
        /////////////////////////////////////////////////////////////////////////////////////
        //////////////////////PYRAMID////////////////////////////////////////////////////////
        final int HEIGHT = 7;
        for (int i = 1; i <= HEIGHT; i++) {
            // Pattern C
            // print leading spaces first
            for (int j = 1; j <= HEIGHT-i; j++) {
                System.out.print("  ");
            }
            for (int j = i; j > 0; j--) {
                System.out.printf("%d ", j);
            }
            // Pattern A
            for (int j = 2; j <= i; j++) {
                System.out.printf("%d ", j);
            }
            System.out.println();
        }
        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////FOUR PATTERNS///////////////////////////////////////
        final int HEIGHT = 6;
        System.out.println("=== Pattern A");
        for (int i = 1; i <= HEIGHT; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.printf("%d ", j);
            }
            System.out.println();
        }
        System.out.println("=== Pattern B version 1");
        for (int i = HEIGHT; i > 0; i--) {
            for (int j = 1; j <= i; j++) {
                System.out.printf("%d ", j);
            }
            System.out.println();
        }
        System.out.println("=== Pattern B version 2");
        for (int i = 1; i <= HEIGHT; i++) {
            for (int j = 1; j <= HEIGHT-i + 1; j++) {
                System.out.printf("%d ", j);
            }
            System.out.println();
        }
        System.out.println("=== Pattern C");
        for (int i = 1; i <= HEIGHT; i++) {
            // print leading spaces first
            for (int j = 1; j <= HEIGHT-i; j++) {
                System.out.print("__");
            }
            for (int j = 1; j <= i; j++) {
                System.out.printf("%d ", j);
            }
            System.out.println();
        }
        System.out.println("=== Pattern D");
        for (int i = 1; i <= HEIGHT; i++) {
            for (int j = 1; j < i; j++) {
                System.out.printf("__");
            }
            for (int j = 1; j <= HEIGHT-i + 1; j++) {
                System.out.printf("%d ", j);
            }
            System.out.println();
        }
        ////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////COMPUTE PI////////////////////////////////////////////////////////
         final int ITERATIONS = 100000000;
        double pi = 0;
        int mul = 1;
        for (int i = 1; i <= ITERATIONS; i += 2) {
            pi += mul*1.0d/i;
            mul = mul == 1 ? -1 : 1; // switch from 1 to -1 and from -1 to 1
        }
        pi *= 4;
        System.out.printf("After %d iterations PI=%f\n", ITERATIONS, pi);
        ////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////HIGHEST SCORE//////////////////////////////////////////////
        Scanner input = new Scanner(System.in);
        System.out.print("How many students? ");
        int count = input.nextInt();
        input.nextLine(); // consume the left over newline character
        // TODO: count must be non-negative
        int bestScore = 0;
        String studentWithBestScore = "";
        for (int i = 0; i < count; i++) {
            System.out.print("Enter student's name: ");
            String name = input.nextLine();
            System.out.print("Enter student's score 0-100: ");
            int scorePerc = input.nextInt();
            input.nextLine(); // consume the left over newline character
            // TODO: verify score 0-100 and name not empty            
            if (scorePerc > bestScore) {
                bestScore = scorePerc;
                studentWithBestScore = name;
            }
        }
        // display the student with highest score
        System.out.printf("Student %s has the best score %d\n", studentWithBestScore, bestScore);
        ////////////////////////////////////////////////////////////////////////////////
        ////////////////////HIGHEST SCORE ARRAY VERSION/////////////////////////////////
        Scanner input = new Scanner(System.in);
        System.out.print("How many students? ");
        int count = input.nextInt();
        input.nextLine(); // consume the left over newline character
        //
        if (count < 1) {
            System.out.println("Error: count must be 1 or greater");
            System.exit(1);
        }
        //
        String[] studentNamesArray = new String[count];
        int[] studentScoresArray = new int[count];
        //
        for (int i = 0; i < count; i++) {
            System.out.print("Enter student's name: ");
            String name = input.nextLine();
            System.out.print("Enter student's score 0-100: ");
            int scorePerc = input.nextInt();
            input.nextLine(); // consume the left over newline character
            // TODO: verify score 0-100 and name not empty
            if (name.isEmpty() || scorePerc < 0 || scorePerc > 100) {
                System.out.println("Error: name must not be empty and score must be 0-100");
                System.exit(1);
            }
            studentNamesArray[i] = name;
            studentScoresArray[i] = scorePerc;
        }
        //
        int bestScore = studentScoresArray[0];
        String studentWithBestScore = studentNamesArray[0];
        for (int i = 0; i < count; i++) {
            int studentScore = studentScoresArray[i];
            if (studentScore > bestScore) {
                bestScore = studentScore;
                studentWithBestScore = studentNamesArray[i];
            }
        }
        // display the student with highest score
        System.out.printf("Student %s has the best score %d\n", studentWithBestScore, bestScore);
        //////////////////////////////////////////////////////////////////////////////////////
        /////////////////////CELSIUS TO FAHRENHEIGHT//////////////////////////////////////////
            double fahrenheit = 9.0/5 * celsius + 32;
        return fahrenheit;
    }
    
    public static double fahrenheitToCelsius(double fahrenheit) {
        return 5.0/9 * (fahrenheit - 32);
    }
    
    public static void main(String[] args) {
        double celsius = 40;
        double fahrenheit = 120;
        System.out.println("Celsius    Fahrenheit     |   Fahrenheit     Celsius");
        System.out.println("------------------------------------------------------------");
        for (int i = 0; i < 10; i++) {
            double fahFromCel = celsiusToFahrenheit(celsius);
            double celFromFah = fahrenheitToCelsius(fahrenheit);
            System.out.printf("%5.1f        %5.1f        |      %5.1f        %5.2f\n",
                    celsius, fahFromCel, fahrenheit, celFromFah);
            celsius -= 1;
            fahrenheit -= 10;
        }
        ///////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////STUDENT GRADES RELATIVE///////////////////////////////////////////
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the number of students: ");
        int count = input.nextInt();
        // TODO: check count is > 0
        if (count < 1) {
            System.out.println("Error: must have some students");
            System.exit(1);
        }
        int[] scoresArray = new int[count];
        for (int i = 0; i < count; i++) {
            System.out.printf("Enter score %d, should be 0-100: ", i + 1);
            int score = input.nextInt();
            // TODO: check score is 0-100            
            if (score < 0 || score > 100) {
                System.out.println("Error: score must be 0-100");
                System.exit(1);
            }
            scoresArray[i] = score;
        }
        // find max value
        // int maxScore = scoresArray[(int)(Math.random()*count)];
        // int maxScore = -1;
        int maxScore = scoresArray[0];
        for (int i = 0; i < count; i++) {
            int score = scoresArray[i];
            if (score > maxScore) {
                maxScore = score;
            }
        }
        // Version 1
        {
            for (int i = 0; i < count; i++) {
                int score = scoresArray[i];
                String grade = "";
                if (score >= maxScore - 10) {
                    grade = "A";
                } else if (score >= maxScore - 20) {
                    grade = "B";
                } else if (score >= maxScore - 30) {
                    grade = "C";
                } else if (score >= maxScore - 40) {
                    grade = "D";
                } else {
                    grade = "F";
                }
                System.out.printf("Student %d score is %d and grade is %s\n", i, score, grade);
            }
        }
        // Version 2 - using a method
        for (int i = 0; i < count; i++) {
            int score = scoresArray[i];
            String grade = gradeNumToLetterRelative(score, maxScore);
            System.out.printf("Student %d score is %d and grade is %s\n", i, score, grade);
        }
        //////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////NAMES ARRAY//////////////////////////////////////////////
        Scanner input = new Scanner(System.in);
        String [] namesArray = new String[4];
        for (int i = 0; i < namesArray.length; i++) {
            System.out.print("Enter friend's name: ");
            String name = input.nextLine();
            namesArray[i] = name;
        }
        for (int i = 1; i <= namesArray.length; i++) {
            System.out.printf("Friend #%d: %s\n", i, namesArray[i-1]);
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////NUMBER ARRAY 3 PARTS//////////////////////////////////////////
        Scanner input = new Scanner(System.in);
        int [] valsArray = new int[4];
        // receive inputs
        for (int i = 0; i < valsArray.length; i++) {
            System.out.print("Enter a value: ");
            int val = input.nextInt();
            input.nextLine(); // consume the left-over newline
            valsArray[i] = val;
        }
        // display back to the user
        for (int i = 0; i < valsArray.length; i++) {
            System.out.printf("Value #%d is %d\n", i + 1, valsArray[i]);
        }
        // find smallest, sum, and average of values
        int sum = 0;
        int smallest = valsArray[0];
        // int smallset = Integer.MIN_VALUE; // okay - start with the smallest possible integer
        // int smallest = 0; // WRONG!!! what if all numbers entered are positive?
        for (int i = 0; i < valsArray.length; i++) {
            int val = valsArray[i];
            sum += val;
            if (val < smallest) {
                smallest = val;
            }
        }
        System.out.println("Smallest number is: " + smallest);
        System.out.println("The sum is : " + sum);
        double avg = (double) sum / valsArray.length;
        System.out.printf("The average is : %.2f\n", avg);
        ///////////////////////////////////////////////////////////////////////////////
        /////////////NUMBER SUM AVERAGE////////////////////////////////////////////////
        Scanner input = new Scanner(System.in);
        int sum = 0, count = 0;
        while (true) {
            System.out.print("Enter an integer number, 0 to finish: ");
            int number = input.nextInt();
            input.nextLine(); // consume the left-over newline
            if (number == 0) {
                break;
            }
            count++;
            sum += number;
        }
        if (count == 0) {
            System.out.println("No data was entered. Exiting.");
            System.exit(0);
        }
        double avg = (double)sum / count;
        System.out.printf("Sum is %d and avg is %.2f\n", sum, avg);
        // System.out.println("Average is " + sum / count);
        ////////////////////////////////////////////////////////////////////////////////////
        ///////////RANDOM NUMBERS////////////////////////////////////////////////////////
        Scanner input = new Scanner(System.in);
        // receive inputs
        System.out.print("Enter minumum: ");
        int min = input.nextInt();
        input.nextLine(); // consume the left-over newline
        
        System.out.print("Enter maximum: ");
        int max = input.nextInt();
        input.nextLine(); // consume the left-over newline

        System.out.print("How many to generate? ");
        int count = input.nextInt();
        input.nextLine(); // consume the left-over newline
        // verify
        if (min > max) {
            System.out.println("Error: minimum can't be greater than maximum");
            System.exit(1);
        }
        if (count < 0) {
            System.out.println("Error: Count must not be negative");
            System.exit(1);
        }
        
        System.out.print("Random number: ");
        for (int i = 0; i < count; i++) {
            int value = (int)(Math.random()*(max-min+1))+min;
            System.out.printf("%s%d", i == 0 ? "" : ", " , value);
        }
        System.out.println();
        ////////////////////////////////////////////////////////////////////////////
        //////////////////SCORE AVERAGRES ARRAYS//////////////////////////////////////////
        ArrayList<Double> scoresList = new ArrayList<>();
        while (true) {
            System.out.print("Enter a score (floating point), negative to end: ");
            double value = input.nextDouble();
            if (value < 0) {
                break;
            }
            scoresList.add(value); // append at the end
        }
        //
        if (scoresList.isEmpty()) {
            System.out.println("No data to work with. Exiting");
            System.exit(0);
        }
        //
        double sum = 0;
        for (double v : scoresList) {
            sum += v;
        }
        double avg = sum / scoresList.size();
        //
        for (double v : scoresList) {
            String relation = v > avg ? "greater" : (v == avg ? "equal" : "lower");
            System.out.printf("%.2f is %s than average which is %.2f\n", v, relation, avg);
        }
        //.add(), .get(), .set(), .remove(), .clear, .size
        double itemIndex3 = scoresList.get(3);
        scoresList.set(2, 3.3); // set item index=2 to value 3.3. Item must already exist!
        scoresList.add(3, 5.5); // insert item at index=3 and set it to 5.5, further items shifted to the right        
        scoresList.remove(1); // delete item at index=1, further items shifted left
        //
        int lower = 0, equal = 0, greater = 0;
        for (double v : scoresList) {
            lower += v < avg ? 1 : 0;
            equal += v == avg ? 1 : 0;
            greater += v > avg ? 1 : 0;
        }
        System.out.printf("The average is %.2f\n", avg);
        System.out.printf("There %d number(s) lower than the average\n", lower);
        System.out.printf("There %d number(s) equal to the average\n", equal);
        System.out.printf("There %d number(s) greater than the average\n", greater);
    }
    
}
